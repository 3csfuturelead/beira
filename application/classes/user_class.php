<?php

class user {

     static function get_countries() {
          global $db;
          $sql = "SELECT id,name
				  FROM tblcountry 
				 ORDER BY name ASC";
          $result = $db->fetch_all_array($sql);
          return $result;
     }
     static function get_mailadmin() {
          global $db;
          $sql = "SELECT id,name,email
				  FROM tblinvcontacts
				 ORDER BY name ASC";
          $result = $db->fetch_all_array($sql);
          return $result;
     }

     static function get_active_admin_emails() {
          global $db;
          $sql = "SELECT *
				  FROM tbladminemails WHERE activated =1";
          $result = $db->fetch_all_array($sql);
          return $result;
     }

     function valid_input_text($input) {
          #checks whether the input string contains non-allowed special characters
          $input = trim($input);
          if ((strstr($input, "#")) || (strstr($input, "!")) || (strstr($input, "|")) || (strstr($input, "~")) || (strstr($input, ";")) || (strstr($input, "^")) || (strstr($input, "`")) || (strstr($input, "<"))) {
               #contains restricted character(s)
               return false;
          } else {
               #contains no restricted characters
               return true;
          }
     }

     //check syntactic validity of an email address
     function valid_email($checkemail) {
          //preg_match("/^([0-9a-z]([-_.]?[0-9a-z])*@[0-9a-z]([-.]?[0-9a-z])*\\.[a-wyz][a-z](fo|g|l|m|mes|o|op|pa|ro|seum|t|u|v|z)?)$/i", $checkemail)
          if (preg_match("/^[a-z0-9]+([_\\.-][a-z0-9]+)*@([a-z0-9]+([\.-][a-z0-9]+)*)+\\.[a-z]{2,}$/i", $checkemail)) {
               return true;
          } else {
               return false;
          }
     }

     function validtelephone($telephone) {
          $flag = 0;
          $telephone1 = str_split(trim($telephone));
          $valid_characters = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "+", "-", "-", " ", "", ")", "(");
          foreach ($telephone1 as $val) {
               if (!in_array($val, $valid_characters)) {
                    $flag = 1;
               }
          }

          if ($flag == 1) {
               return false;
          } else {
               return true;
          }
     }

     static function get_country_by_id($id) {

          global $db;
          $record = $db->query_first("SELECT  id,name FROM tblcountry WHERE id=" . $id . "");
          return $record;
     }

}

?>
