<?php
class getData{
    static function getCountryName_byId($id){
        global $db;
        $country    =   $db->query_first("SELECT name FROM tblcountries WHERE id=".$id);
        return $country['name'];
    }
    static function getAllMainmenu_Items(){
        global $db;
        $menu   =   $db->fetch_all_array("SELECT * FROM tblmain_content WHERE status=1 ORDER BY display_order");
        return $menu;
    }

    static function getALLCountryName(){
        global $db;
        $country    =   $db->fetch_all_array("SELECT id,name FROM tblcountries ORDER BY name");
        return $country;
    }

    static function getMainmenu_Items($id){
        global $db;
        $menu   =   $db->fetch_all_array("SELECT * FROM tblmain_content WHERE maincontent_id in ($id) AND status=1 ORDER BY display_order");
        return $menu;
    }

    static function getMainmenuItemDetails($id){
        global $db;
        $menu   =   $db->query_first("SELECT * FROM tblmain_content WHERE maincontent_id =".$id."");
        return $menu;
    }

    static function getSubContent_byMainID($id) {
        global $db;
        $menu   =   $db->fetch_all_array("SELECT * FROM tblsub_content WHERE  status=1 AND maincontent_id=".$id."  ORDER BY display_order");
        return $menu;
    }

    static function getSubotherContent_bysubID($id) {
        global $db;
        $menu   =   $db->fetch_all_array("SELECT * FROM tblsubother_content WHERE  status=1 AND subcontent_id=".$id."");
        return $menu;
    }

    static function getSubotherContent_bysubID_limited($id) {
        global $db;
        $menu   =   $db->fetch_all_array("SELECT * FROM tblsubother_content WHERE status=1 AND subcontent_id=".$id." LIMIT 0,3");
        return $menu;
    }

    static function getBreadcum($type,$val){
        global $db;

        switch ($type) {
            case 01:
                $sql="SELECT `maincontent_id`,`heading` FROM `tblmain_content` WHERE `maincontent_id`='".$val."'";
                $data=$db->query_first($sql);
                //$breadcrum="&nbsp;>&nbsp;<a href='home.php?id=".base64_encode($data['maincontent_id'])."'>".$data['heading']." </a>";
                $breadcrum="&nbsp;>&nbsp;".$data['heading'];
                break;

            case 02:
                $sql="SELECT `m`.`maincontent_id` as `mid`,`m`.`heading` as `mname`,`s1`.`subcontent_id` as `s1id`,`s1`.`heading` as `s1name` FROM `tblmain_content` `m` JOIN `tblsub_content` `s1` ON `s1`.`maincontent_id`=`m`.`maincontent_id` WHERE `s1`.`subcontent_id`='".$val."'";
                $data=$db->query_first($sql);
                $breadcrum="&nbsp;>&nbsp;<a href='home.php?id=".$data['mid']."'>".$data['mname']."</a>&nbsp;>&nbsp;".$data['s1name'];
                //$breadcrum="&nbsp;>&nbsp;<a href='home.php?main=".base64_encode($data['mid'])."'>".$data['mname']."</a>&nbsp;>&nbsp;<a href='home.php?sid=".base64_encode($data['s1id'])."'>".$data['s1name']." </a>";
                break;

            case 03:
                $sql="SELECT `m`.`maincontent_id` as `mid`,`m`.`heading` as `mname`,`s1`.`subcontent_id` as `s1id`,`s1`.`heading` as `s1name`,`s2`.`subothercontent_id` as `s2id`,`s2`.`heading` as `s2name` FROM `tblmain_content` `m` JOIN `tblsub_content` `s1` ON `s1`.`maincontent_id`=`m`.`maincontent_id` JOIN `tblsubother_content` `s2` ON `s1`.`subcontent_id`=`s2`.`subcontent_id` WHERE `s2`.`subothercontent_id`='".$val."'";
                $data=$db->query_first($sql);
                $breadcrum="&nbsp;>&nbsp;<a href='home.php?id=".$data['mid']."'>".$data['mname']." </a>&nbsp;>&nbsp;<a href='home.php?sid=".$data['s1id']."'>".$data['s1name']." </a>&nbsp;>&nbsp;".$data['s2name'];
                //$breadcrum="&nbsp;>&nbsp;<a href='home.php?id=".base64_encode($data['mid'])."'>".$data['mname']." </a>&nbsp;>&nbsp;<a href='home.php?sid=".base64_encode($data['s1id'])."'>".$data['s1name']." </a>&nbsp;>&nbsp;<a href='home.php?soid=".base64_encode($data['s2id'])."'>".$data['s2name']." </a>";
                break;

            case 04:
                $sql="SELECT `m`.`maincontent_id` as `mid`,`m`.`heading` as `mname`,`s1`.`subcontent_id` as `s1id`,`s1`.`heading` as `s1name`,`s2`.`subothercontent_id` as `s2id`,`s2`.`heading` as `s2name`,`s3`.`subother_other_content_id` as `s3id`,`s3`.`heading` as `s3name` FROM `tblmain_content` `m` JOIN `tblsub_content` `s1` ON `s1`.`maincontent_id`=`m`.`maincontent_id` JOIN `tblsubother_content` `s2` ON `s1`.`subcontent_id`=`s2`.`subcontent_id` JOIN `tblsubother_other_content` `s3` ON `s3`.`subothercontent_id`=`s2`.`subothercontent_id` WHERE `s3`.`subother_other_content_id`='".$val."'";
                $data=$db->query_first($sql);
                $breadcrum="&nbsp;>&nbsp;<a href='home.php?id=".$data['mid']."'>".$data['mname']."</a>&nbsp;>&nbsp;<a href='home.php?sid=".$data['s1id']."'>".$data['s1name']." </a>&nbsp;>&nbsp;<a href='home.php?soid=".$data['s2id']."'>".$data['s2name']."</a>&nbsp;>&nbsp;".$data['s3name'];
                //$breadcrum="&nbsp;>&nbsp;<a href='home.php?id=".base64_encode($data['mid'])."'>".$data['mname']."</a>&nbsp;>&nbsp;<a href='home.php?sid=".base64_encode($data['s1id'])."'>".$data['s1name']." </a>&nbsp;>&nbsp;<a href='home.php?soid=".base64_encode($data['s2id'])."'>".$data['s2name']."</a>&nbsp;>&nbsp;<a href='home.php?sooid=".base64_encode($data['s3id'])."'>".$data['s3name']."</a>";
                break;

            default:
                break;
        }

        return $breadcrum;

    }

    static function getgalleryimages($id){
        global $db;
        $data   =   $db->fetch_all_array("SELECT name FROM tblsolutiongallery WHERE status=1 AND cat_id=".$id." ORDER BY display_order");
        return $data;
    }

    static function getMainSitedata() {
        global $db;
        $data   =   $db->query_first("SELECT * FROM  tblmaindetails WHERE id=1");
        return $data;
    }


    static function get_sideMenu($option,$id) {
        global $db;
        switch ($option) {
            case 01:
                $sql="SELECT maincontent_id as id,heading FROM tblmain_content WHERE status=1 AND maincontent_id in ($id)  ORDER BY display_order";
                $data=$db->fetch_all_array($sql);
                break;

            case 02:
                $sql="SELECT subcontent_id as id,heading FROM tblsub_content WHERE status=1 AND maincontent_id=".$id." ORDER BY display_order";
                $data=$db->fetch_all_array($sql);
                break;

            case 03:
                $sql="SELECT subothercontent_id as id,heading FROM tblsubother_content WHERE status=1 AND subcontent_id=".$id." ORDER BY display_order";
                $data=$db->fetch_all_array($sql);
                break;

            case 04:
                $sql="SELECT subother_other_content_id as id,heading FROM tblsubother_other_content WHERE status=1 AND subothercontent_id=".$id." ORDER BY display_order";
                $data=$db->fetch_all_array($sql);
                break;

            default:
                break;
        }

        return $data;
    }

    static function getWether() {
        $location = "ASI|LK|CE009|COLOMBO";  //http://pastebin.com/dbtemx5F
        $metric = 1;  //0 f - 1 c
        $url = 'http://wwwa.accuweather.com/adcbin/forecastfox/weather_data.asp?location=' . $location . '&metric=' . $metric;
        $timeout = 0;

        $ch = curl_init() or die(curl_error());
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $file_contents = curl_exec($ch);
        curl_close($ch);
        $xml = simplexml_load_string($file_contents);

        $weather['city']            = (string)$xml->local->city;
        $weather['time']            = (string)$xml->local->time;
        $weather['curr_temp']       = (int)$xml->currentconditions->temperature;
        $weather['curr_text']       = (string)$xml->currentconditions->weathertext;
        $weather['curr_icon']       = (int)$xml->currentconditions->weathericon;

        return $weather;
    }


        static function getRoomName_byId($id){
            global $db;
            $room   =   $db->query_first("SELECT name FROM tblroom_types WHERE id=".$id);
            //echo "<br>SELECT `t`.`name` FROM `tab_res_rates` `r` INNER JOIN `tab_room_types` `t` ON `r`.`room_id`=`t`.`id`  WHERE `r`.`id`=$id<br>";
            return $room['name'];
        }

        static function getotherchargesData(){
            global $db;
            $data    =   $db->fetch_all_array("SELECT * FROM tblotherdata WHERE id!=1 AND status=1");
            return $data;
        }

        static function getotherchargesName_byID($id){
            global $db;
            $data    =   $db->query_first("SELECT ratename FROM tblotherdata WHERE id=".$id);
            return $data['ratename'];
        }


        static function getairportrate(){
            global $db;
            $data    =   $db->query_first("SELECT * FROM tblotherdata WHERE id=1");
            return $data;
        }

        static function getMealName_byId($id){
            global $db;
            $meal    =   $db->query_first("SELECT name FROM tblmeal_type WHERE id=".$id);
            return $meal['name'];
        }

        static function getMealRate_byId($id){
            global $db;
            $meal    =   $db->query_first("SELECT charge FROM tblmeal_type WHERE id=".$id);
            return $meal['charge'];
        }


        static function getAll_Mealtypes(){
            global $db;
            $meal    =   $db->fetch_all_array("SELECT id,name FROM tblmeal_type WHERE status=1 ORDER BY name DESC");
            return $meal;
        }

        static function getbasic_Mealtypes(){
            global $db;
            $meal    =   $db->fetch_all_array("SELECT id,name FROM tblmeal_type WHERE status=1 AND id!=4 ORDER BY name DESC");
            return $meal;
        }

        static function getBedName_byId($id){
            global $db;
            $policy    =   $db->query_first("SELECT name FROM tblbed_type WHERE id=".$id);
            return $policy['name'];
        }

        static function getBedtype_byId($id) {
            global $db;
            $bed    =   $db->query_first("SELECT name FROM  tblbed_type WHERE id=".$id);
            return $bed['name'];
        }


        static function getBedData(){
            global $db;
            $policy    =   $db->fetch_all_array("SELECT * FROM tblbed_type WHERE status=1 ORDER BY id");
            return $policy;
        }

        static function getCurrency_type_ById($id) {
            global $db;
            $menu   =   $db->query_first("SELECT display_name FROM tblcurrency_types WHERE id =".$id."");
            return $menu['display_name'];
        }


        static function getAll_roomnames(){
            global $db;
            $menu   =   $db->fetch_all_array("SELECT name,id,image FROM tblroom_types WHERE status=1 ORDER BY name");
            return $menu;
        }

        static function getRoomdata_byId($id){
            global $db;
            $menu   =   $db->query_first("SELECT * FROM tblroom_types WHERE status=1 AND id=".$id." ORDER BY name");
            return $menu;
        }


        static function getcharges(){
            global $db;
            $data   =   $db->query_first("SELECT * FROM tblhotel_data WHERE id=1");
            return $data;
        }

        static function getpromotioncategorydata($id){
            global $db;
            $data   =   $db->query_first("SELECT * FROM  tblpromotion_categories WHERE id=".$id);
            return $data;
        }

        static function getAllpermission_categories() {
            global $db;
            $menu   =   $db->fetch_all_array("SELECT * FROM tblpermission_catagory ORDER BY catagory");
            return $menu;
        }

        static function getpermission_leveldata($id) {
            global $db;
            $data   =   $db->query_first("SELECT * FROM tblpermission_level WHERE id=".$id);
            return $data;
        }

        static function getroledata($id) {
            global $db;
            $data   =   $db->query_first("SELECT * FROM tblgroups WHERE id=".$id);
            return $data;
        }

        static function getUserGroup_data() {
            global $db;
            $menu   =   $db->fetch_all_array("SELECT * FROM tblgroups WHERE status=1");
            return $menu;
        }

        static function getUserGroup_data_byID($id) {
            global $db;
            $menu   =   $db->query_first("SELECT * FROM tblgroups WHERE id=".$id);
            return $menu;
        }

        static function getAllActivepermission_categories() {
            global $db;
            $menu   =   $db->fetch_all_array("SELECT * FROM tblpermission_catagory WHERE status=1 ORDER BY catagory");
            return $menu;
        }

        static function getpermission_leveldata_byCategory($id) {
            global $db;
            $data   =   $db->fetch_all_array("SELECT * FROM tblpermission_level WHERE status=1 AND catagory=".$id);
            return $data;
        }

        static function getUserdata_byID($id) {
            global $db;
            $menu   =   $db->query_first("SELECT * FROM tblusers WHERE id=".$id);
            return $menu;
        }

        static function getSubCategoriesByMainCatId($id) {
        global $db;
        $products   =   $db->fetch_all_array("SELECT * FROM tblproduct_subcategory WHERE status=1 AND category_id=".$id);
        return $products;
        }

        static function getProductsBySubCatId($id) {
        global $db;
        $products   =   $db->fetch_all_array("SELECT * FROM tblproducts WHERE subcategory_id=$id ORDER BY display_order ASC");
        return $products;
        }

}
?>
