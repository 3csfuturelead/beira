<?php

class Permission {

    var $page_id = "";
    var $premission_tag = "";
    var $all_permission = "";

    function check_permission($db) {

        if ($_SESSION['admin']['user_level'] == '1') {
            return true;
        } else { // new 
            $admin_id = $_SESSION['admin']['id'];
            /*
            $permissions = "SELECT permissions FROM tblpermission WHERE user_id ='" . $admin_id . "'"; //to get permission by user level ID
            $all_permission = $db->query_first($permissions);
            */
            
            $admin_group_id=$db->query_first("SELECT level FROM tblusers WHERE id = $admin_id");
            $admin_group_id = $admin_group_id['level'];
            
            $permissions = "SELECT permissions FROM tblgroup_permissions WHERE group_id ='" . $admin_group_id . "'"; //to get permission by user level ID
            $all_permission = $db->query_first($permissions);
            
            $permission_id = "SELECT id FROM tblpermission_level WHERE page_name ='" . $this->premission_tag . "'"; //to get page id
            $user_levels = $db->query_first($permission_id);
            
            if ($user_levels) {
                $page_id = $user_levels['id'];
            }
            if ($all_permission) {
                if ($user_levels) {
                    $pieces = explode(",", $all_permission['permissions']);
                    foreach ($pieces as $per) {
                        if ($per == $page_id) {
                            return true;
                        }
                    }
                }
            }
            return false;
        } // new
    }

    function checkMenu($permission_tag) {
        global $db;

        $per_tag = new Permission;
        $per_tag->premission_tag = $permission_tag;
        $permission_block = $per_tag->check_permission($db);

        return $permission_block;
    }

    function checkBlockPermissions($args = array()) {
        foreach ($args as $arg) {
            if (checkMenu($arg)) {
                return true;
            }
        }

        return false;
    }

}

?>