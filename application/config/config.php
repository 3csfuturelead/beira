<?php

// database connection config
define('DB_SERVER', "localhost");
define('DB_USER', "root");
define('DB_PASS', "");
define('DB_DATABASE', "beira_local");


define('HTTP_PATH', 'http://beira.local/');
define('DOC_ROOT', str_replace('\\', '/', pathinfo(__FILE__,PATHINFO_DIRNAME).'/../'));


//	TABLE HEADER COLOR
$BGCOLOR_HEADERS = "#3c8dbc";

//	TABLE ROW COLORS
$BGCOLOR_ROWS = "#f3f3f3";

//	BREAD DRUMBS
$BREADCRUMB_COLOR = "#f7e4b7";

//	BREAD DRUMBS BACKGROUND COLOR
$PATH_BACKGROUND_COLOR = "#999999";