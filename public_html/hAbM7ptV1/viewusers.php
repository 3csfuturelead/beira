<?php

require_once('admin.php');

$per_tag = new Permission;
$per_tag->premission_tag = "modify_rates";
$permission_block = $per_tag->check_permission($db);
if (!$permission_block) {
    $INCLUDE_FILE = "permission_denied.php";
    require_once('template_main.php');
    exit();
}

$err = '';
$name = "";
$uname = "";
$description = "";
$level = "";

if (isset($_GET) && isset($_GET['eid']) && is_numeric($_GET['eid'])) {
    $temp_heading = "Edit User Details";
    $id = clean_text($_GET['eid']);
    $data = getData::getUserdata_byID($id);
    $name = $data['name'];
    $uname = $data['username'];
    $description = $data['description'];
    $level = $data['level'];
} else {
    $temp_heading = "Add User";
}


if (isset($_GET['action'], $_GET['did'])) {
    if ($_GET['action'] == 'delete' && is_numeric($_GET['did'])) {
        $userId = $_GET['did'];
        $row = $db->query_first("SELECT id FROM tblusers WHERE id = $userId");
        $userId = $row['id'];

        if (empty($userId)) {
            $err.='Invalid User Selected';
        } else {
            $db->query("DELETE from tblusers WHERE id = $userId");
            header('location:' . $_SERVER['PHP_SELF'] . '?msg=' . base64_encode(8) . '');
        }
    } else {
        $err.='Invalid request';
    }
}

$all_datas = $db->fetch_all_array("SELECT * FROM tblusers ORDER BY name ");
$params = array(
    'mode' => 'Sliding',
    'perPage' => 10,
    'delta' => 1,
    'itemData' => $all_datas
);
$pager = & Pager::factory($params);
$datas = $pager->getPageData();



$temp_heading = "View Users";
$page_main_heading = '<i class="fa fa-wrench"></i>&nbsp;&nbsp;'.'Administrative';
$breaddrum = "<li class='active'><span class='divider'>/</span>All Users</li>";
$INCLUDE_FILE = "includes/viewusers.tpl.php";
//name 	description 	email 	username 	password 	added_date 	active 	level 
require_once('template_main.php');
?>