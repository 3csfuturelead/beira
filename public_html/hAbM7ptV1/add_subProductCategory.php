<?php

require_once('admin.php');
//-----------permissions-------

$per_tag = new Permission;
$per_tag->premission_tag = "Modify Product";
$permission_block = $per_tag->check_permission($db);
if (!$permission_block) {
    $INCLUDE_FILE = "permission_denied.php";
    require_once('template_main.php');
    exit();
}


$heading = "";
$special_heading = "";
$special_wording = "";
$category_id = "";


if ($_POST == true) {
    $err = "";

    $heading = trim($_POST['heading']);
    $special_heading = trim($_POST['special_heading']);
    $special_wording = trim($_POST['special_wording']);
    $category_id = trim($_POST['category_id']);

    if ($heading == "") {
        $err = $err . "<li>Please enter heading</li>";
    }

    if ($special_heading == "") {
        $err = $err . "<li>Please enter special heading</li>";
    }

    /*if ($special_wording == "") {
        $err = $err . "<li>Please enter special wording</li>";
    }*/

    if($category_id == ""){
        $err = $err . "<li>Please select a category</li>";
    }

    if ($err == "") {
        $data_arr = array();
        $data_arr['heading'] = $heading;
        $data_arr['special_heading'] = $special_heading;
        $data_arr['special_wording'] = $special_wording;
        $data_arr['category_id'] = $category_id;;
        $data_arr['update_date'] = date("Y-m-d");


        if (isset($_POST['btnadd']) == true) {

            $insert_id = $db->query_insert("tblproduct_subcategory", $data_arr);
            if ($insert_id) {
                //**************** generate log entry *******************
                $logString = "Add Sub Product Category id - " . $insert_id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************
                header("Location:".$_SERVER['PHP_SELF']."?msg=" . base64_encode(7) . "");
                exit;
            } else {
                $err = $err . "Not inserted";
            }
        }

        if (isset($_POST['btnedit']) == true) {

            $id=$_POST["id"];

            $update_id = $db->query_update("tblproduct_subcategory", $data_arr,"id=".$id);
            if ($update_id) {
                //**************** generate log entry *******************
                $logString = "Update Sub Category id - " . $id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************
                header("Location:".$_SERVER['PHP_SELF']."?id=$id&msg=" . base64_encode(6) . "");
                exit;
            } else {
                $err = $err . "Not inserted";
            }
        }
    }
}


$category_array = $db->fetch_all_array("SELECT * FROM tblproduct_category ORDER BY display_order  ASC");

if (isset($_GET['id']) && is_numeric($_GET['id'])) {
    $id=$_GET['id'];
    $sql="SELECT * FROM tblproduct_subcategory WHERE id=$id";
    $result=$db->query_first($sql);

    $page_headings="Edit Sub Categories";

    if ($result) {
        $heading = $result['heading'];
        $special_heading = $result['special_heading'];
        $special_wording = $result['special_wording'];
        $category_id = $result['category_id'];

    } else {
        header("location: dashboard.php?err=".base64_encode(5));
        exit();
    }


} else {
    $page_headings="Add Sub Categories";
}



$page_main_heading = '<i class="fa fa-sitemap"></i>&nbsp;&nbsp;'.$page_headings;

$breaddrum = "<li class='active'>$page_headings</li>";
$INCLUDE_FILE = "includes/add_subProductCategory.tpl.php";
require_once('template_main.php');
?>