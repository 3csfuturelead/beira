$(document).ready(function($) {
    /*add new user modal start */
    $('#frm_add_new_user').validate({
        focusCleanup: true,
        errorElement: "div",
        errorClass: "text-red",
        rules: {
            name: "required",
            uname: {
                required: true,
                minlength: 4
            },
            pw: {
                required: true,
                minlength: 5
            },
            repw: {
                required: true,
                minlength: 5,
                equalTo: "#pw"
            },
            cat: {
                required: true,
            }
        },
        submitHandler: function(form) {
            $.ajax({
                url: form.action,
                type: form.method,
                data: $(form).serialize(),
                success: function(response) {
                    //$('#answers').html(response);
                    //alert(response);
                    $successEle = $('#addNewUser').find('#msg_success');
                    $failedEle = $('#addNewUser').find('#msg_error');
                    clearEffect($successEle, $failedEle);
                    if ($.trim(response) === '2') {
                        $('#addNewUser #msg_error').find('#msg_txt').html(adminMessages(3));
                        messageEffect($successEle, $failedEle);
                    } else if ($.trim(response) === '1') {
                        $('#addNewUser #msg_success').find('#msg_txt').html(adminMessages(1));
                        messageEffect($failedEle, $successEle);
                    } else if ($.trim(response) === '0') {
                        $('#addNewUser #msg_error').find('#msg_txt').html(adminMessages(4));
                        messageEffect($successEle, $failedEle);
                    } else {
                        clearEffect($successEle, $failedEle);
                    }
                }
            });
        }
    });

    /*add new user modal end */

    /*add new slider image start */
    $('#frm_add_new_slider_image').validate({
        focusCleanup: true,
        errorElement: "div",
        errorClass: "text-red",
        ignore: [],
        rules: {
            slider_image:{
                required: true
            }
        },
        submitHandler: function(form) {
            var formData = new FormData($('#frm_add_new_slider_image')[0]);
            $.ajax({
                url : form.action,
                type: form.method,
                data : formData,
                mimeType:"multipart/form-data",
                contentType: false,
                cache: false,
                processData:false,
                success: function(data, textStatus, jqXHR)
                {
                   $successEle = $('#addNewSliderImage').find('#msg_success');
                    $failedEle = $('#addNewSliderImage').find('#msg_error');
                    clearEffect($successEle, $failedEle);
                    if ($.trim(data) === '0') {
                        $('#addNewSliderImage #msg_error').find('#msg_txt').html("Action Failed");
                        messageEffect($successEle, $failedEle);
                    } else if ($.trim(data) === '1') {
                        $('#addNewSliderImage #msg_success').find('#msg_txt').html("Successfully Added");
                        messageEffect($failedEle, $successEle);
                    } else {
                        clearEffect($successEle, $failedEle);
                    }
                }
            });
        }
    });

    /*add new slider image modal end */

    $('#addNewUser,#modifyPermissionLevel,#addnewpermissioncategory').on('show.bs.modal', function(e) {
        $successEle = $(this).find('#msg_success');
        $failedEle = $(this).find('#msg_error');
        clearEffect($successEle, $failedEle);
    });

    /*edit user modal start */

    $('#frm_edit_new_user').validate({
        focusCleanup: true,
        errorElement: "div",
        errorClass: "text-red",
        rules: {
            name: "required",
            uname: {
                required: true,
                minlength: 4
            },
            cat: {
                required: true,
            }
        },
        submitHandler: function(form) {
            $.ajax({
                url: form.action,
                type: form.method,
                data: $(form).serialize(),
                success: function(response) {
                    $successEle = $('#editUser').find('#msg_success');
                    $failedEle = $('#editUser').find('#msg_error');
                    clearEffect($successEle, $failedEle);
                    if ($.trim(response) === '1') {
                        $('#editUser #msg_success').find('#msg_txt').html(adminMessages(2));
                        messageEffect($failedEle, $successEle);
                    } else if ($.trim(response) === '0') {
                        $('#modifyPermissionLevel #msg_error').find('#msg_txt').html(adminMessages(4));
                        messageEffect($successEle, $failedEle);
                    }
                }
            });
        }
    });

    /*edit user modal end */

    /*
     var url = 'js/myAjaxFile.php';
     var type = 'POST';
     var data = {name: "name"};
     var x = getjsondata(url, type, data);
     x.success(function(data) {
     alert(data['advert']);
     });
     x.error(function(){
     alert('error');
     });
     */
//    $('#editUser').on('show.bs.modal', function(e) {
//        console.log($(this));
//        $url = 'ajax_data_request.php';
//        $type = 'POST';
//        var eid = $("[class$=btneditUser]").attr('eid');
//        alert(eid)
//        $data = {
//            eid: "name"
//        };
//        $result = getjsondata($url, $type, $data);
//        $result.success(function(data) {
//            //alert(data['eid']);
//        });
//    });

    /* get user data start*/
    $('.btneditUser').on('click', function() {
        var eid = $(this).attr('eid');
        var actionrequest = $('#editUser').find('#actionrequest').val();
        var url = 'ajax_data_request.php';
        var type = 'POST';
        var data = {
            eid: eid,
            actionrequest: actionrequest
        };

        $successEle = $('#editUser').find('#msg_success');
        $failedEle = $('#editUser').find('#msg_error');
        clearEffect($successEle, $failedEle);
        $("input[type='text']").removeClass('text-red');
        $("input[type='text']").next().remove();
        $result = getjsondata(url, type, data);
        $result.success(function(data) {
            $('#editUser').find('#name').val(data['name']);
            $('#editUser').find('#uname').val(data['username']);
            $('#editUser').find('#desc').val(data['description']);
            $('#editUser').find("select option").each(function() {
                if (data['level'] === $(this).val()) {
                    $(this).attr('selected', 'selected')
                }
            });
            $('#editUser').modal('show');
        });
        $('#editUser').find('#eid').val(eid);
    });
    /* get user data end*/

    /*modify permission level modal start*/

    $('#frm_mod_per_level').validate({
        focusCleanup: true,
        errorElement: "div",
        errorClass: "text-red",
        rules: {
            name: {
                required: true,
                minlength: 3
            },
            desc: {
                required: true,
                minlength: 3
            },
            cat: {
                required: true
            }
        },
        submitHandler: function(form) {
            $.ajax({
                url: form.action,
                type: form.method,
                data: $(form).serialize(),
                success: function(response) {
                    $successEle = $('#modifyPermissionLevel').find('#msg_success');
                    $failedEle = $('#modifyPermissionLevel').find('#msg_error');
                    clearEffect($successEle, $failedEle);
                    if ($.trim(response) === '2') {
                        $('#modifyPermissionLevel #msg_error').find('#msg_txt').html(adminMessages(5));
                        messageEffect($successEle, $failedEle);
                    } else if ($.trim(response) === '1') {
                        $('#modifyPermissionLevel #msg_success').find('#msg_txt').html(adminMessages(1));
                        messageEffect($failedEle, $successEle);
                    } else if ($.trim(response) === '0') {
                        $('#modifyPermissionLevel #msg_error').find('#msg_txt').html(adminMessages(4));
                        messageEffect($successEle, $failedEle);
                    } else {
                        clearEffect($successEle, $failedEle);
                    }
                }
            });
        }
    });

    /*modify permission level modal end*/

    /*get permission level modal data start*/

    $('.btnModifyPermiLevel').on('click', function() {
        var eid = $(this).attr('eid');
        var actionrequest = $('#editmodifyPermissionLevel').find('#actionrequest').val();
        var url = 'ajax_data_request.php';
        var type = 'POST';
        var data = {
            eid: eid,
            actionrequest: actionrequest
        };
        $successEle = $('#editmodifyPermissionLevel').find('#msg_success');
        $failedEle = $('#editmodifyPermissionLevel').find('#msg_error');
        clearEffect($successEle, $failedEle);
        $("input[type='text']").removeClass('text-red');
        $("input[type='text']").next().remove();
        $("select").removeClass('text-red');
        $("select").next('div .text-red').remove();
        $("textarea").removeClass('text-red');
        $("textarea").next().remove();
        $result = getjsondata(url, type, data);
        $result.success(function(data) {
            $('#editmodifyPermissionLevel').find('#name').val(data['page_name']);
            $('#editmodifyPermissionLevel').find('#desc').val(data['description']);
            $('#editmodifyPermissionLevel #permission_cat').find("select option").each(function() {
                if (data['catagory'] === $(this).val()) {
                    $(this).attr('selected', 'selected')
                }
            });
            $('#editmodifyPermissionLevel #permission_state').find("select option").each(function() {
                if (data['status'] === $(this).val()) {
                    $(this).attr('selected', 'selected')
                }
            });
        });
        $('#editmodifyPermissionLevel').find('#eid').val(eid);
    });


    /*get permission level modal data end*/


    /*edit permission level modal data start*/

    $('#frm_edit_mod_per_level').validate({
        focusCleanup: true,
        errorElement: "div",
        errorClass: "text-red",
        rules: {
            name: {
                required: true,
                minlength: 3
            },
            desc: {
                required: true,
                minlength: 3
            },
            cat: {
                required: true
            }
        },
        submitHandler: function(form) {
            $.ajax({
                url: form.action,
                type: form.method,
                data: $(form).serialize(),
                success: function(response) {
                    $successEle = $('#editmodifyPermissionLevel').find('#msg_success');
                    $failedEle = $('#editmodifyPermissionLevel').find('#msg_error');
                    clearEffect($successEle, $failedEle);
                    if ($.trim(response) === '2') {
                        $('#editmodifyPermissionLevel #msg_error').find('#msg_txt').html(adminMessages(5));
                        messageEffect($successEle, $failedEle);
                    } else if ($.trim(response) === '1') {
                        $('#editmodifyPermissionLevel #msg_success').find('#msg_txt').html(adminMessages(2));
                        messageEffect($failedEle, $successEle);
                    } else if ($.trim(response) === '0') {
                        $('#editmodifyPermissionLevel #msg_error').find('#msg_txt').html(adminMessages(4));
                        messageEffect($successEle, $failedEle);
                    } else {
                        clearEffect($successEle, $failedEle);
                    }
                }
            });
        }
    });


    /*edit permission level modal data end*/



    $('#editmodifyPermissionLevel #addnewcategory').on('click', function() {
        $('#editmodifyPermissionLevel').modal('hide');
    });

    $('#editmodifyPermissionLevel').on('show.bs.modal', function(e) {
        $.cookie("location", 2);
    });

    $('#modifyPermissionLevel #addnewcategory').on('click', function() {
        $('#modifyPermissionLevel').modal('hide');
    });

    $('#modifyPermissionLevel').on('show.bs.modal', function() {
        $.cookie("location", 1);
    });

    $('#addnewpermissioncategory').on('hide.bs.modal', function(e) {

        var cookieValue = $.cookie("location");

        if (cookieValue == 1) {
            $('#modifyPermissionLevel').modal('show');
        }
        if (cookieValue == 2) {
            $('#editmodifyPermissionLevel').modal('show');
        }

        $(this).find('#name').val('');
        $(this).find('#name').next('div .text-red').remove();
        $('#modifyPermissionLevel,#editmodifyPermissionLevel').find('#cat').empty();

        var url = "ajax_data_request.php";
        var type = 'POST';
        var data = {
            actionrequest: 'permi_cat'
        };

        $result = gethtmldata(url, type, data);
        $result.success(function(data) {
            $('#modifyPermissionLevel,#editmodifyPermissionLevel').find('#cat').html(data);
        });

    });

    /* add new permission category modal start*/

    $('#frm_add_per_cat').validate({
        focusCleanup: true,
        errorElement: "div",
        errorClass: "text-red",
        rules: {
            name: {
                required: true,
                minlength: 4
            },
            status: {
                required: true,
            }
        },
        submitHandler: function(form) {
            $.ajax({
                url: form.action,
                type: form.method,
                data: $(form).serialize(),
                success: function(response) {
                    $successEle = $('#addnewpermissioncategory').find('#msg_success');
                    $failedEle = $('#addnewpermissioncategory').find('#msg_error');
                    clearEffect($successEle, $failedEle);
                    if ($.trim(response) === '1') {
                        $('#addnewpermissioncategory #msg_success').find('#msg_txt').html(adminMessages(1));
                        messageEffect($failedEle, $successEle);
                    } else if ($.trim(response) === '0') {
                        $('#addnewpermissioncategory #msg_error').find('#msg_txt').html(adminMessages(4));
                        messageEffect($successEle, $failedEle);
                    } else if ($.trim(response) === '2') {
                        $('#addnewpermissioncategory #msg_error').find('#msg_txt').html(adminMessages(6));
                        messageEffect($successEle, $failedEle);
                    }
                }
            });
        }
    });
    /* add new permission category modal end*/


    /* edit permission category modal get data start */

    $('.btneditCategory').on('click', function() {
        var eid = $(this).attr('eid');
        var actionrequest = $('#editpermissioncategory').find('#actionrequest').val();
        var url = 'ajax_data_request.php';
        var type = 'POST';
        var data = {
            eid: eid,
            actionrequest: actionrequest
        };

        $successEle = $('#editpermissioncategory').find('#msg_success');
        $failedEle = $('#editpermissioncategory').find('#msg_error');
        clearEffect($successEle, $failedEle);
        $("input[type='text']").removeClass('text-red');
        $("input[type='text']").next().remove();
        $result = getjsondata(url, type, data);
        $result.success(function(data) {
            $('#editpermissioncategory').find('#name').val(data['catagory']);
            $('#editpermissioncategory').find("select option").each(function() {
                if (data['status'] === $(this).val()) {
                    $(this).attr('selected', 'selected')
                }
            });
            $('#editpermissioncategory').modal('show');
        });
        $('#editpermissioncategory').find('#eid').val(eid);
    });

    /* edit permission category modal get data start */


    /* edit permission category modal start */

    $('#frm_edit_per_cat').validate({
        focusCleanup: true,
        errorElement: "div",
        errorClass: "text-red",
        rules: {
            name: {
                required: true,
                minlength: 4
            },
            status: {
                required: true,
            }
        },
        submitHandler: function(form) {
            $.ajax({
                url: form.action,
                type: form.method,
                data: $(form).serialize(),
                success: function(response) {
                    $successEle = $('#editpermissioncategory').find('#msg_success');
                    $failedEle = $('#editpermissioncategory').find('#msg_error');
                    clearEffect($successEle, $failedEle);
                    if ($.trim(response) === '1') {
                        $('#editpermissioncategory #msg_success').find('#msg_txt').html(adminMessages(2));
                        messageEffect($failedEle, $successEle);
                    } else if ($.trim(response) === '0') {
                        $('#editpermissioncategory #msg_error').find('#msg_txt').html(adminMessages(4));
                        messageEffect($successEle, $failedEle);
                    } else if ($.trim(response) === '2') {
                        $('#editpermissioncategory #msg_error').find('#msg_txt').html(adminMessages(6));
                        messageEffect($successEle, $failedEle);
                    }
                }
            });
        }
    });
    /* edit permission category modal start */

    $('#selectAll').click(function() {
        $('#deselectAll').attr('checked', false);
        this.checked = true;
        if (this.checked) {
            $('.allchecks').each(function() {
                this.checked = true;
            });
        }
    });

    $('#deselectAll').click(function() {
        $('#selectAll').attr('checked', false);
        this.checked = true;
        if (this.checked) {
            $('.allchecks').each(function() {
                this.checked = false;
            });
        }
    });


    /*tinymce start*/
    tinymce.init({
        selector: ".tinyEditor",
        forced_root_block: false,
        plugins: [
            "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons template textcolor paste textcolor"
        ],
        toolbar1: "newdocument | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
        toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | inserttime preview | forecolor backcolor",
        toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft",
        menubar: false,
        toolbar_items_size: 'small',
        style_formats: [
            {title: 'Bold text', inline: 'b'},
            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
            {title: 'Example 1', inline: 'span', classes: 'example1'},
            {title: 'Example 2', inline: 'span', classes: 'example2'},
            {title: 'Table styles'},
            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ],
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ],
        valid_elements: "@[id|class|style|title|dir<ltr?rtl|lang|xml::lang|onclick|ondblclick|"
                + "onmousedown|onmouseup|onmouseover|onmousemove|onmouseout|onkeypress|"
                + "onkeydown|onkeyup],a[rel|rev|charset|hreflang|tabindex|accesskey|type|"
                + "name|href|target|title|class|onfocus|onblur],strong/b,em,i,strike,u,"
                + "#p,-ol[type|compact],-ul[type|compact],-li,br,img[longdesc|usemap|"
                + "src|border|alt=|title|hspace|vspace|width|height|align],-sub,-sup,"
                + "-blockquote,-table[border=0|cellspacing|cellpadding|width|frame|rules|"
                + "height|align|summary|bgcolor|background|bordercolor],-tr[rowspan|width|"
                + "height|align|valign|bgcolor|background|bordercolor],tbody,thead,tfoot,"
                + "#td[colspan|rowspan|width|height|align|valign|bgcolor|background|bordercolor"
                + "|scope],#th[colspan|rowspan|width|height|align|valign|scope],caption,-div,"
                + "-span,-code,-pre,address,-h1,-h2,-h3,-h4,-h5,-h6,hr[size|noshade],-font[face"
                + "|size|color],dd,dl,dt,cite,abbr,acronym,del[datetime|cite],ins[datetime|cite],"
                + "object[classid|width|height|codebase|*],param[name|value|_value],embed[type|width"
                + "|height|src|*],script[src|type],map[name],area[shape|coords|href|alt|target],bdo,"
                + "button,col[align|char|charoff|span|valign|width],colgroup[align|char|charoff|span|"
                + "valign|width],dfn,fieldset,form[action|accept|accept-charset|enctype|method],"
                + "input[accept|alt|checked|disabled|maxlength|name|readonly|size|src|type|value],"
                + "kbd,label[for],legend,noscript,optgroup[label|disabled],option[disabled|label|selected|value],"
                + "q[cite],samp,select[disabled|multiple|name|size],small,"
                + "textarea[cols|rows|disabled|name|readonly],tt,var,big"
    });

    /*tinymce end*/

    $('.btnback').click(function() {
        window.history.back();
    });

    $(function() {
        $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
        $('.tree li.parent_li > span').on('click', function(e) {
            var children = $(this).parent('li.parent_li').find(' > ul > li');
            if (children.is(":visible")) {
                children.hide('fast');
                $(this).attr('title', 'Expand this branch').find(' > i').addClass("fa fa-plus").removeClass("fa fa-minus");
                $(this).attr('title', 'Expand this branch').find(' > i').addClass("fa");

            } else {
                children.show('fast');
                $(this).attr('title', 'Collapse this branch').find(' > i').addClass("fa fa-minus").removeClass("fa fa-plus");
                $(this).attr('title', 'Expand this branch').find(' > i').addClass("fa");
            }
            e.stopPropagation();
        });
    });

    // edit product category ajax
    $(".editcategory_form").on('submit', function(e)
    {


        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        var msgbox = $(".return_alert");
        msgbox.html('<img src="img/ajax-loader.gif">');

        var success_message="<div class=\"alert alert-success\"><a class=\"close\" data-dismiss=\"alert\">&times;</a><strong><span class=\"badge badge-success\">Success!</span></strong> Update successful.</div>";

        var error_message="<div class=\"alert alert-info\"><a class=\"close\" data-dismiss=\"alert\">&times;</a><strong><span class=\"badge badge-error\">Error!</span></strong> Update Failed. Please Try Again!</div>";

        $.ajax({
            url : formURL,
            type: "POST",
            data : postData,
            success: function(data, textStatus, jqXHR)
            {
                if (data==1) {
                    msgbox.html(success_message);
                    msgbox.hide();
                    msgbox.fadeIn(1000);
                } else{
                    msgbox.html(error_message);
                    msgbox.hide();
                    msgbox.fadeIn(1000);
                }
            },
        });

        e.preventDefault(); //Prevent Default action.
        e.unbind();

    });

    $(".editcategory_form").on('submit', function(e)
    {


        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        var msgbox = $(".return_alert");
        msgbox.html('<img src="img/ajax-loader.gif">');

        var success_message="<div class=\"alert alert-success\"><a class=\"close\" data-dismiss=\"alert\">&times;</a><strong><span class=\"badge badge-success\">Success!</span></strong> Update successful.</div>";

        var error_message="<div class=\"alert alert-info\"><a class=\"close\" data-dismiss=\"alert\">&times;</a><strong><span class=\"badge badge-error\">Error!</span></strong> Update Failed. Please Try Again!</div>";

        $.ajax({
            url : formURL,
            type: "POST",
            data : postData,
            success: function(data, textStatus, jqXHR)
            {
                if (data==1) {
                    msgbox.html(success_message);
                    msgbox.hide();
                    msgbox.fadeIn(1000);
                } else{
                    msgbox.html(error_message);
                    msgbox.hide();
                    msgbox.fadeIn(1000);
                }
            },
        });

        e.preventDefault(); //Prevent Default action.
        e.unbind();

    });




    // delegate calls to data-toggle="lightbox"
    $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
        event.preventDefault();
        return $(this).ekkoLightbox({
            onShown: function() {
                if (window.console) {
                    return console.log('');
                }
            }
        });
    });

    $('#main_cat_sel').change();


});

function getjsondata(url, type, data) {
    return $.ajax({
        url: url,
        type: type,
        data: data,
        dataType: 'json',
        /* success: function(result) {
         alert(result['ajax']); // "Hello world!" alerted
         console.log(result['advert']); // The value of your php $row['adverts'] will be displayed
         },
         error: function() {
         console.log('return error');
         }*/
    });
}


function gethtmldata(url, type, data) {
    return $.ajax({
        url: url,
        type: type,
        data: data,
        dataType: 'html',
    });
}

function adminMessages(id) {
    if (id == 1) {
        return 'Successfully Inserted';
    } else if (id == 2) {
        return 'Successfully updated';
    } else if (id == 3) {
        return 'Username Already Takken';
    } else if (id == 4) {
        return 'Internal Error Please Contact Admin';
    } else if (id == 5) {
        return 'Page Name Already in Use';
    } else if (id == 6) {
        return 'Category Name Already in Use';
    }
}

function messageEffect(element1, element2) {
    element1.slideUp();
    element2.slideDown();
}

function clearEffect(element1, element2) {
    element1.slideUp();
    element2.slideUp();
}

function checkImage(picField) {
        var picFile = picField;
        var imagePath = document.getElementById(picFile).value;
        var pathLength = imagePath.length;
        var lastDot = imagePath.lastIndexOf(".");
        var fileType = imagePath.substring(lastDot,pathLength);
        if((fileType == ".jpg") || (fileType == ".JPG") || (fileType == ".jpeg") || (fileType == ".JPEG") || (fileType == ".png") || (fileType == ".PNG") ) {
            return true;
        } else {
            alert(" Invalid file type! You are allowed to upload only JPG/PNG images.");
            document.getElementById(picFile).value = "";
            document.getElementById(picFile).focus();
            return false;
        }
}

function load_subcategories(value,selected_value) {
    $.post('ajax_data_request.php', {actionrequest: 'get_sub_categories',cat_id:value,selected:selected_value}, function(data, textStatus, xhr) {
        $('#sub_cat_sel').html(data);
        $('#sub_cat_sel').change();
    });


}

function load_product_list(value) {
    $.post('ajax_data_request.php', {actionrequest: 'get_product_list',subcat_id:value}, function(data, textStatus, xhr) {
        $('#product_list').html(data);
    });
}


