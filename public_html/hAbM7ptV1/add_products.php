<?php

require_once('admin.php');
//-----------permissions-------

$per_tag = new Permission;
$per_tag->premission_tag = "Modify Product";
$permission_block = $per_tag->check_permission($db);
if (!$permission_block) {
    $INCLUDE_FILE = "permission_denied.php";
    require_once('template_main.php');
    exit();
}


$product_id = "";
$name = "";
$first_desc = "";
$second_desc = "";
$third_desc = "";
$forth_desc = "";
$fifth_desc = "";
$first_image = "";
$second_image = "";
$third_image = "";
$thumbnail = "";
$category_id="";
$subcategory_id="";

if (isset($_GET) && isset($_GET['action'])) {
    if ($_GET['action']=="delete_content_image") {
        if (isset($_GET['id']) && isset($_GET['image']) && isset($_GET['imageno'])) {

            $id = $_GET['id'];
            $image = $_GET['image'];

            $data[$_GET['imageno']] = "";

            $result = $db->query_update("tblproducts", $data, "id=" . $id);

            if (file_exists(DOC_ROOT . 'images/content/products/' . $image)) {
                $unlink = @unlink(DOC_ROOT . 'images/content/products/' . $image);
            }

            //**************** generate log entry *******************
            $logString = "delete product image, id - " . $id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
            $log = Message::log_details($_SESSION['admin']['username'], $logString);
            // **************************************************

            if ($result) {
                header('location:' . $_SERVER['PHP_SELF'] . '?id=' . $id . '&msg=' . base64_encode(8) . '');
                exit;
            } else {
                header('location:' . $_SERVER['PHP_SELF'] . '?id=' . $id . '&msg=' . base64_encode(5) . '');
                exit;
            }
        }
    }
}

if ($_POST == true) {
    $err = "";

    $product_id = trim($_POST['product_id']);
    $name = trim($_POST['name']);
    $first_desc = trim($_POST['first_desc']);
    // $second_desc = trim($_POST['second_desc']);
    // $third_desc = trim($_POST['third_desc']);
    // $forth_desc = trim($_POST['forth_desc']);
    // $fifth_desc = trim($_POST['fifth_desc']);
    $category_id= trim($_POST['category_id']);
    $subcategory_id= trim($_POST['subcategory_id']);

    if ($product_id == "") {
        $err = $err . "<li>Please enter Product ID</li>";
    }

    if ($category_id == "") {
        $err = $err . "<li>Please select a Main Category</li>";
    }

    if ($subcategory_id == "") {
        $err = $err . "<li>Please select a sub category</li>";
    }

    if ($err == "") {
        $data_arr = array();

        $data_arr['product_id'] = $product_id;
        $data_arr['name'] = $name;
        $data_arr['first_desc'] = $first_desc;
        // $data_arr['second_desc'] = $second_desc;
        // $data_arr['third_desc'] = $third_desc;
        // $data_arr['forth_desc'] = $forth_desc;
        // $data_arr['fifth_desc'] = $fifth_desc;
        $data_arr['category_id']= $category_id;
        $data_arr['subcategory_id']= $subcategory_id;
        $data_arr['update_date'] = date("Y-m-d");


        // if (isset($_FILES) && $_FILES['first_image']['name'] != "") {
        //     $temp_upload_image =upload::upload_images(DOC_ROOT.'images/content/products/',$_FILES['first_image']);
        //     $imagename = str_replace(" ", "", $temp_upload_image);
        //     $temp_image_url = DOC_ROOT . 'images/content/products/' . $temp_upload_image;
        //     $new_image_url = DOC_ROOT . 'images/content/products/' . $imagename;
        //     rename($temp_image_url, $new_image_url);
        //     $data_arr['first_image'] = $imagename;
        // }

        // if (isset($_FILES) && $_FILES['second_image']['name'] != "") {
        //     $temp_upload_image =upload::upload_images(DOC_ROOT.'images/content/products/',$_FILES['second_image']);
        //     $imagename = str_replace(" ", "", $temp_upload_image);
        //     $temp_image_url = DOC_ROOT . 'images/content/products/' . $temp_upload_image;
        //     $new_image_url = DOC_ROOT . 'images/content/products/' . $imagename;
        //     rename($temp_image_url, $new_image_url);
        //     $data_arr['second_image'] = $imagename;
        // }

        // if (isset($_FILES) && $_FILES['third_image']['name'] != "") {
        //     $temp_upload_image =upload::upload_images(DOC_ROOT.'images/content/products/',$_FILES['third_image']);
        //     $imagename = str_replace(" ", "", $temp_upload_image);
        //     $temp_image_url = DOC_ROOT . 'images/content/products/' . $temp_upload_image;
        //     $new_image_url = DOC_ROOT . 'images/content/products/' . $imagename;
        //     rename($temp_image_url, $new_image_url);
        //     $data_arr['third_image'] = $imagename;
        // }

        if (isset($_FILES) && $_FILES['thumbnail']['name'] != "") {
            $temp_upload_image =upload::upload_images(DOC_ROOT.'images/content/products/',$_FILES['thumbnail']);
            $imagename = str_replace(" ", "", $temp_upload_image);
            $temp_image_url = DOC_ROOT . 'images/content/products/' . $temp_upload_image;
            $new_image_url = DOC_ROOT . 'images/content/products/' . $imagename;
            rename($temp_image_url, $new_image_url);
            $data_arr['thumbnail'] = $imagename;
        }


        if (isset($_POST['btnadd']) == true) {

            $insert_id = $db->query_insert("tblproducts", $data_arr);
            if ($insert_id) {
                //**************** generate log entry *******************
                $logString = "Add Product  id - " . $insert_id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************
                header("Location:".$_SERVER['PHP_SELF']."?msg=" . base64_encode(7) . "");
                exit;
            } else {
                $err = $err . "Not inserted";
            }
        }

        if (isset($_POST['btnedit']) == true) {

            $id=$_POST["id"];

            $update_id = $db->query_update("tblproducts", $data_arr,"id=".$id);
            if ($update_id) {
                //**************** generate log entry *******************
                $logString = "Update  product id - " . $id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************
                header("Location:".$_SERVER['PHP_SELF']."?id=$id&msg=" . base64_encode(6) . "");
                exit;
            } else {
                $err = $err . "Not inserted";
            }
        }
    }
}


$category_array = $db->fetch_all_array("SELECT * FROM tblproduct_category ORDER BY display_order  ASC");
$subcategory_array = $db->fetch_all_array("SELECT * FROM tblproduct_subcategory ORDER BY display_order  ASC");

if (isset($_GET['id']) && is_numeric($_GET['id'])) {
    $id=$_GET['id'];
    $sql="SELECT * FROM tblproducts WHERE id=$id";
    $result=$db->query_first($sql);

    $page_headings="Edit Product";

    if ($result) {

        $product_id = $result['product_id'];
        $name =$result['name'];
        $first_desc = $result['first_desc'];
        // $second_desc = $result['second_desc'];
        // $third_desc = $result['third_desc'];
        // $forth_desc = $result['forth_desc'];
        // $fifth_desc = $result['fifth_desc'];
        // $first_image = $result['first_image'];
        // $second_image = $result['second_image'];
        // $third_image = $result['third_image'];
        $thumbnail = $result['thumbnail'];
        $category_id= $result['category_id'];
        $subcategory_id= $result['subcategory_id'];

    } else {
        header("location: dashboard.php?err=".base64_encode(5));
        exit();
    }


} else {
    $page_headings="Add Product";
}



$page_main_heading = '<i class="fa fa-download"></i>&nbsp;&nbsp;'.$page_headings;

$breaddrum = "<li class='active'>$page_headings</li>";
$INCLUDE_FILE = "includes/add_products.tpl.php";
require_once('template_main.php');
?>