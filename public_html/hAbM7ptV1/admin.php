<?php

require_once('../top.php');
require_once(DOC_ROOT . 'classes/admin_messages.php');
require_once(DOC_ROOT . 'classes/upload.class.php');
require_once(DOC_ROOT . 'classes/log.class.php');
require_once(DOC_ROOT . 'classes/class_permission.php');
require_once(DOC_ROOT . 'classes/Pager.class.php');
require_once(DOC_ROOT . 'classes/careers_class.php');
require_once(DOC_ROOT . 'classes/mailing.class.php');
require_once(DOC_ROOT . 'classes/content_class.php');

// require_once(DOC_ROOT . 'classes/Davailability_class.php');
require_once(DOC_ROOT . 'classes/get_data.php');
// require_once(DOC_ROOT . 'classes/Drate_class.php');
// require_once(DOC_ROOT . 'classes/Dallotment_class.php');
// require_once(DOC_ROOT . 'classes/showrates.php');

if (!isset($_SESSION['admin']) || $_SESSION['admin']['logged'] != TRUE) {

    header("location: index.php?msg=" . base64_encode(3) . "");
    exit;
} else {

    //check user start
    $sql = "SELECT * FROM tblusers WHERE id='" . $_SESSION['admin']['id'] . "'";
    $user_data = $db->query_first($sql); //get user
    
    if (!$user_data) {
        header("location: index.php?msg=" . base64_encode(3) . "");
        exit;
    }
    
    $sql = "SELECT tg.group_name,tu.added_date FROM tblgroups as tg INNER JOIN tblusers as tu on tu.level=tg.id WHERE tu.id='" . $_SESSION['admin']['id'] . "'";
    $query = $db->query_first($sql);
    $user_role = $query['group_name'];
    $added_date = $query['added_date'];
    $timestamp = strtotime($added_date);
    $added_date = date('M. Y',$timestamp);

    //check user end
    //get message from class -start
    $msg = "";
    $err = "";
    $info = "";
    $war = "";
    if (isset($_GET['msg'])) {
        $msg = Message:: showMsg($_GET['msg']);
    }
    if (isset($_GET['err'])) {
        $err = Message:: showMsg($_GET['err']);
    }
    if (isset($_GET['info'])) {
        $info = Message:: showMsg($_GET['info']);
    }
    if (isset($_GET['war'])) {
        $war = Message:: showMsg($_GET['war']);
    }
    //get message from class -end
}
?>
