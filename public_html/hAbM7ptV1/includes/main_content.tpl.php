<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-title"><i class="fa fa-th-list"></i>&nbsp;&nbsp;Edit Menu Item Details</div>
            </div>
            <div class="box-body">
                <form action="" method="post" enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-md-2"><label class="control-label" for="inputtitle">Title</label></div>
                        <div class="col-md-4">
                            <input type="text" name="heading" id="inputtitle" class="form-control" value="<?php echo $heading; ?>">
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-2"><label class="control-label" for="inputtitle">Special Title</label></div>
                        <div class="col-md-4">
                            <input type="text" name="spe_heading" id="inputtitle" class="form-control" value="<?php echo $spe_heading; ?>">
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-2"><label class="control-label" for="inputtitle">Details</label></div>
                        <div class="col-md-10">
                            <textarea name="details" id="details" rows="15" class="form-control tinyEditor">
                                <?php echo $details; ?>
                            </textarea>
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-2"><label class="control-label" for="inputtitle">Special Details</label></div>
                        <div class="col-md-8">
                            <textarea name="spe_details" rows="15" class="form-control" ><?php echo $spe_details; ?></textarea>
                        </div>
                    </div><br/>
                    <pre>Meta Data</pre>
                    <br/>
                    <div class="row">
                        <div class="col-md-2"><label class="control-label" for="inputtitle">Title</label></div>
                        <div class="col-md-7">
                            <textarea name="meta_title" rows="4" class="form-control"><?php echo $meta_title; ?></textarea>
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-2"><label class="control-label" for="inputtitle">Meta Description</label></div>
                        <div class="col-md-7">
                            <textarea name="meta_desc" rows="4"  class="form-control"><?php echo $meta_desc; ?></textarea>
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-2"><label class="control-label" for="inputtitle">Meta Keywords</label></div>
                        <div class="col-md-7">
                            <textarea name="meta_key" rows="4"  class="form-control"><?php echo $meta_key; ?></textarea>
                        </div>
                    </div>
                    <br/>
                    <pre>Page Images</pre>
                    <br/>
                    <div class="row">
                        <div class="col-md-2"><label class="control-label" for="inputtitle">Main Banner Image</label></div>
                        <div class="col-md-4">
                            <?php if (!file_exists("../images/content/banner/$image1") || $image1 == "") { ?>
                                <input type="file" name="image1" id="image1" class="" onchange="return checkImage('image1')">
                            <?php } else { ?>
                                <a href="../images/content/banner/<?php echo $image1; ?>" data-toggle="lightbox"> <!--data-title="A random title" data-footer="A custom footer text"-->
                                    <img src="../images/content/banner/<?php echo $image1; ?>" class="img-responsive">
                                </a>
                                <a class="btn btn-danger btn-flat btn-block" href="<?php echo $_SERVER['PHP_SELF'] . '?action=delete_content_image&maincontent_id=' . $maincontent_id . '&image=' . $image1 . '&imageno=image1'; ?>" onclick="return confirm('Are you sure to delete this image?');"><i class="fa fa-eraser"></i>&nbsp;&nbsp;Delete</a>
                            <?php } ?>
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-2"><label class="control-label" for="inputtitle">Special Banner Image</label></div>
                        <div class="col-md-4">
                            <?php if (!file_exists("../images/content/banner/$image2") || $image2 == "") { ?>
                                <input type="file" name="image2" id="image2" class="" onchange="return checkImage('image2')">
                            <?php } else { ?>
                                <a href="../images/content/banner/<?php echo $image2; ?>" data-toggle="lightbox">
                                    <img src="../images/content/banner/<?php echo $image2; ?>" class="img-responsive">
                                </a>

                                <a class="btn btn-danger btn-flat btn-block" href="<?php echo $_SERVER['PHP_SELF'] . '?action=delete_content_image&maincontent_id=' . $maincontent_id . '&image=' . $image2 . '&imageno=image2'; ?>" onclick="return confirm('Are you sure to delete this image?');"><i class="fa fa-eraser"></i>&nbsp;&nbsp;Delete</a>
                            <?php } ?>
                        </div>
                    </div><br/>

                    <div class="row">
                        <div class="col-md-2"><label class="control-label" for="inputtitle">Thumbnail Image</label></div>
                        <div class="col-md-4">
                            <?php if (!file_exists("../images/content/banner/$image3") || $image3 == "") { ?>
                                <input type="file" name="image3" id="image3" class="" onchange="return checkImage('image3')">
                            <?php } else { ?>

                                <a href="../images/content/banner/<?php echo $image3; ?>" data-toggle="lightbox">
                                    <img src="../images/content/banner/<?php echo $image3; ?>" class="img-responsive">
                                </a>

                                <a class="btn btn-danger btn-flat btn-block" href="<?php echo $_SERVER['PHP_SELF'] . '?action=delete_content_image&maincontent_id=' . $maincontent_id . '&image=' . $image3 . '&imageno=image3'; ?>" onclick="return confirm('Are you sure to delete this image?');"><i class="fa fa-eraser"></i>&nbsp;&nbsp;Delete</a>
                            <?php } ?>
                        </div>
                    </div>
                    <br/>


                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <label class="checkbox">
                                <input type="hidden" name="maincontent_id" value="<?php echo $maincontent_id; ?>" />
                            </label>
                            <button type="submit" name="btnsave" class="btn btn-primary btn-flat btn-block"><i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;Save Details</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-2 col-md-offset-10">
                        <button class="btn btn-default btn-flat btn-block btnback"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (sizeof($sub_contents) < 1) { ?>
    <div class="alert alert-info">
        <strong>Sorry !</strong>&nbsp;&nbsp;&nbsp;No Sub content available.<a class="btn btn-default btn-flat btn-block" href="sub_content.php?maincontent_id=<?php echo $maincontent_id; ?>" ><i class="fa fa-plus"></i>&nbsp;&nbsp;Add Sub Content</a>
    </div>
<?php } else { ?>
    <!--accordian-->
    <div class="bs-docs-example">
        <div id="accordion2" class="accordion">

            <div class="accordion-group">
                <div class="accordion-heading">
                    <a href="#collapseone" data-parent="#accordion2" data-toggle="collapse" class="accordion-toggle collapsed btn btn-info btn-flat btn-block" >
                        <b class="icon-tasks"></b>&nbsp;&nbsp;&nbsp;Sub Items
                    </a>
                </div>
                <div class="accordion-body collapse" id="collapseone" style="height: 0px;">
                    <div class="accordion-inner">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box box-info">

                                    <div class="box-body">
                                        <!--accordian-->
                                        <form name="frmorder" action="" method="post">
                                            <table class="table table-condensed">
                                                <thead>
                                                <br/>
                                                <tr>
                                                    <th width="35%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" ><?php echo $heading; ?> Listings</th>
                                                    <th width="10%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >Added date</th>
                                                    <th width="15%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >Status</th>
                                                    <th width="20%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >&nbsp;</th>
                                                    <th width="10%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >Priority</th>
                                                    <th width="10%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >&nbsp;</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $i = 0;
                                                    foreach ($sub_contents as $row) {
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $row['heading']; ?></td>
                                                            <td><?php echo $row['added_date']; ?></td>
                                                            <td>
                                                                <div class="btn-group">
                                                                    <button class="btn btn-primary btn-flat"><?php
                                                                        if ($row['status'] == 1) {
                                                                            echo "Active";
                                                                        } else {
                                                                            echo "Inactive";
                                                                        }
                                                                        ?></button>
                                                                    <button class="btn btn-primary btn-flat dropdown-toggle" data-toggle="dropdown">
                                                                        <span class="caret"></span>
                                                                        <span class="sr-only">Toggle Dropdown</span>
                                                                    </button>
                                                                    <ul class="dropdown-menu">
                                                                        <!-- dropdown menu links -->
                                                                        <li>
                                                                            <?php if ($row['status'] == 1) {
                                                                                ?>
                                                                                <a href="<?php echo $_SERVER['PHP_SELF'] . '?action=change_content_status&status=0&subcontent_id=' . $row['subcontent_id'] . '&maincontent_id=' . $maincontent_id . ''; ?>" onclick="return confirm('Are you sure to inactive this content?');" >Change to Inactive</a>
                                                                                <?php
                                                                            } else {
                                                                                ?>
                                                                                <a href="<?php echo $_SERVER['PHP_SELF'] . '?action=change_content_status&status=1&subcontent_id=' . $row['subcontent_id'] . '&maincontent_id=' . $maincontent_id . ''; ?>"  onclick="return confirm('Are you sure to active this content?');" >Change to Active</a>
                                                                            <?php } ?>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <a href="sub_content.php?maincontent_id=<?php echo $maincontent_id; ?>&subcontent_id=<?php echo $row['subcontent_id']; ?>" class="btn btn-default btn-flat" ><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit</a>

                                                                <a href="<?php echo $_SERVER['PHP_SELF'] . '?action=delete_subcontent&subcontent_id=' . $row['subcontent_id'] . '&maincontent_id=' . $maincontent_id; ?>" onclick="return confirm('Are you sure to delete this content?');" class=" btn btn-danger btn-flat" ><i class="fa fa-eraser"></i>&nbsp;&nbsp;Delete</a>
                                                            </td>
                                                            <td>
                                                                <select name="display_order[<?php echo $row['subcontent_id']; ?>]"  class="form-control" onchange="setsub_order(this.value,<?php echo $row['subcontent_id']; ?>)" >
                                                                    <?php for ($j = 1; $j <= sizeof($sub_contents); $j++) { ?>
                                                                        <option value="<?php echo $j; ?>" <?php
                                                                                if ($row['display_order'] == $j) {
                                                                                    echo "selected";
                                                                                }
                                                                                ?>><?php echo $j; ?></option>
                                                                                <?php } ?>
                                                                </select>
                                                                <span id="status_sub<?php echo $row['subcontent_id']; ?>" class="label label-success flat"></span>

                                                            </td>
                                                            <td ><a href="subother_content.php?subcontent_id=<?php echo $row['subcontent_id']; ?>" >Add sub</a></td>
                                                        </tr>
                                                            <?php $i++;
                                                        }
                                                        ?>
                                                </tbody>
                                            </table>
                                        </form>
                                        <!--accordian-->
                                    </div>
                                    <div class="box-footer">
                                        <div class="col-md-2 col-md-offset-8"></div>
                                        <a class="btn btn-primary btn-flat" href="sub_content.php?maincontent_id=<?php echo $maincontent_id; ?>" ><i class="fa fa-plus"></i>&nbsp;&nbsp;Add Sub Content</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--accordian-->
<?php } ?>


    <script>

    function setsub_order(value, subid) {
        var msgbox = $("#status_sub" + subid);
        $("#status_sub" + subid).html('<img src="img/ajax-loader.gif">');
        $.ajax({
            type: "POST",
            url: "set_sub_order.php",
            data: "subid=" + subid + "&value=" + value + "&action=sub",
            success: function(msg) {
                msgbox.html(msg);
                msgbox.hide();
                msgbox.fadeIn(1000);
                msgbox.fadeOut(1500);
            }

        });

    }

    function setslider_order(value, image_id) {
        var msgbox = $("#status_sliderimage_" + image_id);
        $("#status_sliderimage_" + image_id).html('<img src="img/ajax-loader.gif">');
        $.ajax({
            type: "POST",
            url: "set_sub_order.php",
            data: "image_id=" + image_id + "&value=" + value + "&action=slider",
            success: function(msg) {
                msgbox.html(msg);
                msgbox.hide();
                msgbox.fadeIn(1000);
                msgbox.fadeOut(1500);
            }

        });

    }
</script>





<!-- add new Slider Images Modal start-->
<div class="modal fade" id="addNewSliderImage" tabindex="-1" role="dialog" aria-labelledby="addNewSliderImageLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="addNewSliderImageLabel"><i class="fa fa-user"></i>&nbsp;&nbsp;Add Slider Image</h4>
            </div>
            <form name="frm_add_new_slider_image" id="frm_add_new_slider_image" enctype="multipart/form-data" action="ajax_data.php" method="post" role="form">
                <div id="msg_success" hidden="hidden">
                    <div class="alert alert-success ">
                        <i class="fa fa-check"></i>
                        <div id="msg_txt"></div>
                    </div>
                </div>
                <div id="msg_error" hidden="hidden">
                    <div class="alert alert-danger">
                        <i class="fa fa-check"></i>
                        <div id="msg_txt"></div>
                    </div>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-3"><label>Image</label></div>
                        <div class="col-md-4 form-control">
                            <input type="file"  name="slider_image" id="slider_image"  />
                            <input type="hidden" name="maincontent_id" value="<?php echo $maincontent_id ; ?>">
                        </div>
                    </div><br/>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-flat">Add Images</button>
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                </div>
                <input type="hidden" name="actionrequest" value="addSliderImage"/>
            </form>
        </div>
    </div>
</div>
<!-- add new Slider Images Modal end -->


 <!-- slider images list -->
 <pre>Brief Page Image Slider</pre>
<br/>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div style="margin-right: 115px;"><button class="btn btn-primary btn-flat" data-toggle="modal" data-target="#addNewSliderImage"><i class="fa fa-user"></i>&nbsp;&nbsp;Add New Image</button></div>
    </div>
</div>
<br>
<?php if (sizeof($slider_image_array) < 1) { ?>
    <div class="alert alert-info">
        <strong>Sorry !</strong>&nbsp;&nbsp;&nbsp;No Slider Images available.
    </div>
<?php } else { ?>
    <!--accordian-->
    <div class="bs-docs-example">
        <div id="sliderimage_accordion" class="accordion">

            <div class="accordion-group">
                <div class="accordion-heading">
                    <a href="#slider_image_collapse" data-parent="#sliderimage_accordion" data-toggle="collapse" class="accordion-toggle collapsed btn btn-info btn-flat btn-block" >
                        <b class="icon-tasks"></b>&nbsp;&nbsp;&nbsp;Show Slider Images
                    </a>
                </div>
                <div class="accordion-body collapse" id="slider_image_collapse" style="height: 0px;">
                    <div class="accordion-inner">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box box-info">

                                    <div class="box-body">
                                        <!--accordian-->
                                        <form name="frmorder" action="" method="post">

                                        <div class="row">
                                            <div class="col-md-3" style="background-color: #3c8dbc;color:#FFF"><strong><?php echo $heading; ?> Slider Image</strong></div>
                                            <div class="col-md-2" style="background-color: #3c8dbc;color:#FFF"><strong>Status</strong></div>
                                            <div class="col-md-2" style="background-color: #3c8dbc;color:#FFF"><strong>Display Order</strong></div>
                                        </div>
                                        <br>
                                            <?php
                                            $i = 0;
                                            foreach ($slider_image_array as $row) {
                                                ?>
                                                <?php if (file_exists("../images/content/slider/".$row['image_name']) || $row['image_name'] != "") { ?>

                                                    <div class="row">
                                                    <div class="col-md-3">
                                                        <a href="../images/content/slider/<?php echo $row['image_name']; ?>" data-toggle="lightbox">
                                                            <img src="../images/content/slider/<?php echo $row['image_name']; ?>" class="img-responsive">
                                                        </a>
                                                        <a class="btn btn-danger btn-flat btn-block"  href="<?php echo $_SERVER['PHP_SELF'] . '?action=delete_slider_image&image_id=' . $row['image_id'] . '&image=' . $row['image_name'] . '&maincontent_id=' . $maincontent_id; ?>" onclick="return confirm('Are you sure to delete this image?');"><i class="fa fa-eraser"></i>&nbsp;&nbsp;Delete</a>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <div class="btn-group">
                                                            <button class="btn btn-primary btn-flat"><?php
                                                                if ($row['status'] == 1) {
                                                                    echo "Active";
                                                                } else {
                                                                    echo "Inactive";
                                                                }
                                                                ?></button>
                                                            <button class="btn btn-primary btn-flat dropdown-toggle" data-toggle="dropdown">
                                                                <span class="caret"></span>
                                                                <span class="sr-only">Toggle Dropdown</span>
                                                            </button>
                                                            <ul class="dropdown-menu">
                                                                <!-- dropdown menu links -->
                                                                <li>
                                                                    <?php if ($row['status'] == 1) {
                                                                        ?>
                                                                        <a href="<?php echo $_SERVER['PHP_SELF'] . '?action=change_sliderimage_status&status=0&image_id=' . $row['image_id'] . '&maincontent_id=' . $maincontent_id . ''; ?>" onclick="return confirm('Are you sure to inactive this image?');" >Change to Inactive</a>
                                                                        <?php
                                                                    } else {
                                                                        ?>
                                                                        <a href="<?php echo $_SERVER['PHP_SELF'] . '?action=change_sliderimage_status&status=1&image_id=' . $row['image_id'] . '&maincontent_id=' . $maincontent_id . ''; ?>"  onclick="return confirm('Are you sure to active this image?');" >Change to Active</a>
                                                                    <?php } ?>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <select name="display_order_slider[<?php echo $row['image_id']; ?>]"  class="form-control" onchange="setslider_order(this.value,<?php echo $row['image_id']; ?>)" >
                                                            <?php for ($j = 1; $j <= sizeof($slider_image_array); $j++) { ?>
                                                                <option value="<?php echo $j; ?>" <?php
                                                                        if ($row['display_order'] == $j) {
                                                                            echo "selected";
                                                                        }
                                                                        ?>><?php echo $j; ?></option>
                                                                        <?php } ?>
                                                        </select>
                                                        <span id="status_sliderimage_<?php echo $row['image_id']; ?>" class="label label-success flat"></span>

                                                    </div>
                                                </div>
                                                <br>
                                                <?php } ?>

                                                    <?php $i++;
                                                }
                                                ?>

                                        </form>
                                        <!--accordian-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--accordian-->
<?php } ?>
<!-- slide images accordian ends -->