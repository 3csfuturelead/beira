<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-title"><i class="fa fa-th-list"></i>&nbsp;&nbsp;Sub Other Pages</div>
            </div>
            <div class="box-body">
                <?php if (sizeof($subother_contents) < 1) {
                    ?>
                    <div class="alert"><strong>Sorry !</strong>&nbsp;&nbsp;&nbsp;No Data Found.</div>
                    <?php
                } else {
                    ?>
                    <table class="table table-condensed" style="width:100%;">
                        <thead>

                            <tr>
                                <th width="40%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >Name</th>
                                <th width="10%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >Added date</th>
                                <th width="10%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >Status</th>
                                <th width="20%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >&nbsp;</th>
                                <th width="12%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >Priority</th>
                                <th width="8%" bgcolor="<?php echo $BGCOLOR_HEADERS; ?>" >&nbsp;</th>
                            </tr>
                        </thead>  
                        <tbody>
                            <?php foreach ($subother_contents as $subother) { ?>

                                <tr>
                                    <td ><?php echo $subother['heading']; ?></td>
                                    <td ><?php echo $subother['added_date']; ?></td>
                                    <td >
                                        <div class="btn-group">
                                            <button class="btn btn-primary btn-flat"><?php
                                                if ($subother['status'] == 1) {
                                                    echo "Active";
                                                } else {
                                                    echo "Inactive";
                                                }
                                                ?></button>
                                            <button class="btn btn-primary btn-flat dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <!-- dropdown menu links -->
                                                <li>
                                                    <?php if ($subother['status'] == 1) {
                                                        ?>
                                                        <a href="<?php echo $_SERVER['PHP_SELF'] . '?action=change_subothercontent_status&status=0&subothercontent_id=' . $subother['subothercontent_id'] . '&maincontent_id=' . $subother['maincontent_id']; ?>&subcontent_id=<?php echo $_GET['subcontent_id']; ?>" onclick="return confirm('Are you sure to inactive this content?');" >Change to Inactive</a>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <a href="<?php echo $_SERVER['PHP_SELF'] . '?action=change_subothercontent_status&status=1&subothercontent_id=' . $subother['subothercontent_id'] . '&maincontent_id=' . $subother['maincontent_id']; ?>&subcontent_id=<?php echo $_GET['subcontent_id']; ?>"  onclick="return confirm('Are you sure to active this content?');" >Change to Active</a>
                                                    <?php }
                                                    ?> 
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="subother_content.php?subcontent_id=<?php echo $subother['subcontent_id']; ?>&subothercontent_id=<?php echo $subother['subothercontent_id']; ?>" class="btn btn-default btn-flat" ><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit</a>
                                        &nbsp;
                                        <a href="<?php echo $_SERVER['PHP_SELF'] . '?action=delete_subothercontent&subothercontent_id=' . $subother['subothercontent_id'] . '&maincontent_id=' . $subother['maincontent_id']; ?>&subcontent_id=<?php echo $_GET['subcontent_id']; ?>" onclick="return confirm('Are you sure to delete this content?');" class=" btn btn-danger btn-flat" ><i class="fa fa-eraser"></i>&nbsp;&nbsp;Delete</a>	
                                    </td>
                                    <td>
                                        <select name="display_subotherorder[<?php echo $subother['subothercontent_id']; ?>]"  id="display_subotherorder[<?php echo $subother['subothercontent_id']; ?>]"  class="form-control" onchange="setsubother_order(this.value,<?php echo $subother['subothercontent_id']; ?>)" >
                                            <?php for ($k = 1; $k <= sizeof($subother_contents); $k++) { ?>
                                                <option value="<?php echo $k; ?>" <?php
                                                if ($subother['display_order'] == $k) {
                                                    echo "selected";
                                                }
                                                ?>><?php echo $k; ?></option>
                                                    <?php } ?>
                                        </select>	
                                        <span id="status<?php echo $subother['subothercontent_id']; ?>" class="label label-success flat"></span>		
                                    </td>
                                    <td >&nbsp;</td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php
                }
                ?>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-2 col-md-offset-10">
                        <button class="btn btn-default btn-flat btn-block btnback"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</button>
                    </div>
                </div>   
            </div>
        </div>
    </div>
</div>
<script>
    function setsubother_order(value, subid) {
        var msgbox = $("#status" + subid);
        $("#status" + subid).html('<img src="images/loader.gif">');
        $.ajax({
            type: "POST",
            url: "set_sub_order.php",
            data: "subid=" + subid + "&value=" + value + "&action=subother",
            success: function(msg) {
                msgbox.html(msg);
                msgbox.hide();
                msgbox.fadeIn(1000);
                msgbox.fadeOut(1500);
            }
        });
    }
</script>