<?php

require_once('admin.php');
//-----------permissions-------

$per_tag = new Permission;
$per_tag->premission_tag = "Modify Product";
$permission_block = $per_tag->check_permission($db);
if (!$permission_block) {
    $INCLUDE_FILE = "permission_denied.php";
    require_once('template_main.php');
    exit();
}

if (isset($_GET['action'])) {

    switch ($_GET['action']) {

        case 'status_change':

            $active = $_GET['active'];
            $id = $_GET['id'];
            $cat_id = $_GET['cat_id'];
            $cat_title = $_GET['title'];

            $data = array();
            $data['status'] = $active;
            $result = $db->query_update("tblproduct_subcategory", $data, "id=" . $id);

            if ($result) {
                //**************** generate log entry *******************
                $logString = "Change sub Category status - Category ID = " . $id . " / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************
                header('location:' . $_SERVER['PHP_SELF'] . '?cat_id='.$cat_id.'&title='.$cat_title.'&msg=' . base64_encode(6) . '');
                exit;
            } else {
                header('location:' . $_SERVER['PHP_SELF'] . '?cat_id='.$cat_id.'&title='.$cat_title.'&msg=' . base64_encode(5) . '');
                exit;
            }

            break;

        case 'delete_subcategory':

            $id = $_GET['id'];
            $cat_id = $_GET['cat_id'];
            $cat_title = $_GET['title'];

            $result = $db->query("DELETE FROM tblproduct_subcategory WHERE id =" . $id . "");

            if ($result) {
            //**************** generate log entry *******************
            $logString = "Deleted Sub Category - Category ID = " . $id . " / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
            $log = Message::log_details($_SESSION['admin']['username'], $logString);
            // **************************************************

            header('location:' . $_SERVER['PHP_SELF'] . '?cat_id='.$cat_id.'&title='.$cat_title.'&msg=' . base64_encode(8) . '');
            exit;
            } else{
                header('location:' . $_SERVER['PHP_SELF'] . '?cat_id='.$cat_id.'&title='.$cat_title.'&msg=' . base64_encode(5) . '');
                exit;
            }

            break;
    }
}

if (isset($_GET['cat_id']) && is_numeric($_GET['cat_id'])) {
    $cat_id=$_GET['cat_id'];
    $cat_title = (isset($_GET['title'])) ? $_GET['title'] : "" ;

    $content_link = $db->fetch_all_array("SELECT * FROM tblproduct_subcategory WHERE category_id=$cat_id ORDER BY display_order  ASC");

} else {
    header("Location: dashboard.php?err=".base64_encode(5));
    exit();
}


$page_main_heading = $cat_title .' <i class="fa fa-arrow-circle-right"></i>  View Sub Categories';

$breaddrum = "<li><a href='add_productCategory.php' >$cat_title</a></li><li class='active'>View Sub Categories</li>";
$INCLUDE_FILE = "includes/view_subProductCategory.tpl.php";
require_once('template_main.php');
?>