<?php

require_once('admin.php');

$per_tag = new Permission;
$per_tag->premission_tag = "permissionset";
$permission_block = $per_tag->check_permission($db);
if (!$permission_block) {
    $INCLUDE_FILE = "permission_denied.php";
    require_once('template_main.php');
    exit();
}

if (isset($_GET['type']) && is_numeric($_GET['type']) && isset($_GET['id']) && is_numeric($_GET['id'])) {
    $type = clean_text($_GET['type']);
    $id = clean_text($_GET['id']);
} else {
    header("location:index.php");
}


if (isset($_POST['save_permission']) || isset($_POST['reset_permission'])) {
    if (isset($_POST['permissiondata']) && sizeof($_POST['permissiondata']) > 0) {
        $permissionspost = $_POST['permissiondata'];
        $permissionset = "";
        foreach ($permissionspost as $permissiondata) {
            $permissionset .= $permissiondata . ",";
        }
        $permissionset = rtrim($permissionset, ',');
        
        $type = $_POST['type'];
        $id = $_POST['id'];
        $data_permissions = array();
        //$data_permissions['permissions'] = $permissionset;
        //$data_permissions['permissions'] = $permissionset;
       
        
        if ($type == 1) {
             $data_permissions['permission'] = $permissionset;
            $dataexits = $db->query_first("SELECT COUNT(id) as sum FROM tblpermission WHERE user_id=" . $id);
            if ($dataexits['sum'] > 0) {
                $result = $db->query_update("tblpermission", $data_permissions, "user_id=" . $id);
            } else {
                $data_permissions['added_date'] = date("Y-m-d h:m:s");
                $data_permissions['user_id'] = $id;
                $result = $db->query_insert("tblpermission", $data_permissions);
            }
            if ($result) {
                //**************** generate log entry *******************
                $logString = "Updated User permissions,User id- " . $id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************
                header("Location:user_rolls.php?msg=" . base64_encode(6) . "");
                exit;
            } else {
                header("Location:user_rolls.php?err=" . base64_encode(5) . "");
                exit;
            }
        } else {
            $data_permissions['permissions'] = $permissionset;
            $dataexits = $db->query_first("SELECT COUNT(id) as sum FROM tblgroup_permissions WHERE group_id=" . $id);
            if ($dataexits['sum'] > 0) {
                $result = $db->query_update("tblgroup_permissions", $data_permissions, "group_id=" . $id);
            } else {
                $data_permissions['added_date'] = date("Y-m-d h:m:s");
                $data_permissions['group_id'] = $id;
                $result = $db->query_insert("tblgroup_permissions", $data_permissions);
            }

            if ($result) {
                //**************** generate log entry *******************
                $logString = "Updated Group permissions,Group id- " . $id . "  / USER - " . $_SESSION['admin']['username'] . " ID - " . $_SESSION['admin']['id'];
                $log = Message::log_details($_SESSION['admin']['username'], $logString);
                // **************************************************
                header("Location:user_rolls.php?msg=" . base64_encode(6) . "");
                exit;
            } else {
                header("Location:user_rolls.php?err=" . base64_encode(5) . "");
                exit;
            }
        }
    } else {
        $error = "Please Select atleast 1 Permission Level";
    }
}

if ($type == 2) {
    $breaddrum = "<li class='active'>Group Permission Levels</li>";
} else {
    $breaddrum = "<li class='active'>User Permission Levels</li>";
}
$page_main_heading = '<i class="fa fa-wrench"></i>&nbsp;&nbsp;'.'Administrative';
$INCLUDE_FILE = "includes/permissionset.tpl.php";

require_once('template_main.php');
?>