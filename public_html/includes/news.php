<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container">
    <div class="row" style="max-width: 100%;height: auto;">
		<div class="col-md-12">
			<!-- News Item 1 Start -->
			<div class="panel panel-primary" style="margin-top:25px;">
				<div class="panel-heading">
					<h3 class="panel-title">Kataragama Festival PET clean-up drive “Gives Back Life” to 3 Tonnes of PET Bottles. A range of durable cleaning products to be manufactured from the collected PET bottles.</h3>
					<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-down"></i></span>
				</div>
                <div class="panel-collapse collapse out">
				    <div class="panel-body">
				    	<a href="http://www.dailymirror.lk/article/Coca-Cola-Gives-Back-Life-to-tonnes-of-PET-bottles-in-Katharagama--134964.html" title="The collective team of volunteers comprising of the local authorities and the Kataragama Environmental Society (from the Kataragama Committee), Beira Enviro Solutions and Coca-Cola Beverages Sri Lanka Ltd." target="_blank"><img src="http://static.dailymirror.lk/media/images/image_1503050687-502a8f8e63.jpg" alt="The collective team of volunteers comprising of the local authorities and the Kataragama Environmental Society (from the Kataragama Committee), Beira Enviro Solutions and Coca-Cola Beverages Sri Lanka Ltd." title="The collective team of volunteers comprising of the local authorities and the Kataragama Environmental Society (from the Kataragama Committee), Beira Enviro Solutions and Coca-Cola Beverages Sri Lanka Ltd." style="width: 300px; height: 200px; float: left; margin-right: 10px;"></a>
				    	<p style="text-align: justify;"><span style="font-weight: 600; font-style: italic;">Thursday, 17 August 2017, Colombo:</span> Coca-Cola Beverages Sri Lanka Ltd recently supported and sponsored the effective clean-up of PET bottles following the large crowds gathered at the Kataragama Festival. It was a community-driven sustainable initiative with the partnership between Coca-Cola, Beira Enviro Solutions Ltd (part of BPPL Holdings) and the Kataragama Committee to collect and recycle PET during and after the Kataragama Festival from 24 th July till 10 th August 2017.</p><br>
						<p style="text-align: justify;">Bottled water, fruit juices and carbonated soft drinks are packaged in 100% recyclable PET plastic bottles. Since PET is completely recyclable, Coca-Cola partnered with Beira to “Give Back Life” to these used, empty PET bottles which would otherwise have possibly journeyed to garbage dump sites. Coca-Cola’s extended producer responsibility ensured education, awareness and capacity-building initiatives towards an individual’s responsibility towards recycling, as well as the importance of community-driven, sustainable initiatives in PET collection, recycling and waste management practices.</p><br style="clear: right;">
						<p style="text-align: justify; margin-top: 10px;">The widespread garbage littering witnessed during the days of the Festival reflects poorly on one of the most important cultural and religious events in the country. The PET clean-up drive not only improved the immediate surroundings but also encouraged festival participants to take individual initiative. Ensuring an end-to- end approach to the littering issue during the festivities, 1000 liter and 5000 liter bins were placed in easily accessible locations around the venue for the festival participants to responsibly dispose of their PET bottles. It was a completely hands-on initiative by the organisers with volunteers from Coca-Cola participating on the day to lead the awareness programme in driving recognition of the universal symbol for the PET package – the number ‘1’ in the recycling symbol.</p><br>
						<div style="float: right; margin-left: 10px; height: 203px;">
							<a href="http://www.dailymirror.lk/article/Coca-Cola-Gives-Back-Life-to-tonnes-of-PET-bottles-in-Katharagama--134964.html" title="PET bottles collected by the volunteers" target="_blank"><img src="http://static.dailymirror.lk/media/images/image_1503050758-442205176e.jpg" alt="PET bottles collected by the volunteers" title="PET bottles collected by the volunteers" style="width: 300px; height: 200px;"></a>
							<a href="http://www.dailymirror.lk/article/Coca-Cola-Gives-Back-Life-to-tonnes-of-PET-bottles-in-Katharagama--134964.html" title="Upcycled products made from PET bottles" target="_blank"><img src="http://static.dailymirror.lk/media/images/image_1503050786-c45ee4a257.jpg" alt="Upcycled products made from PET bottles" title="Upcycled products made from PET bottles" style="width: 300px; height: 200px;"></a>						
						</div>						
						<p style="text-align: justify;">Coca-Cola sales associates who oversee the Kataragama area (under the Matara Sales Area) joined hands with volunteers from Beira and the Kataragama Environmental Society in the clean-up efforts of the Kataragama Festival, with the PET collection being managed by Beira. To ensure true environmental responsibility and sustainability, all items in the clean-up drive had to be recycled or reused including gloves and poly-bags. The PET collection was facilitated on the days with the mobilisation of lorries - and this allowed for an astonishing 3 tonnes of PET to be collected.</p><br>						
						<p style="text-align: justify;">Mayank Arora, Managing Director of Coca-Cola Beverages Sri Lanka Ltd said “Coca-Cola in Sri Lanka is committed to promoting PET collection and recycling initiatives in Sri Lanka. We are thankful to all the volunteers who dedicated their time to drive this important initiative and took action to preserve their environment. Giving back Life to 3,000 Kilograms of PET bottles will be accomplished when the collected PET is upcycled to manufacture a range of durable cleaning products for both professional and household applications by Beira Enviro Solutions.”</p><br>						
						<p style="text-align: justify;">Dr. Anush Amarasinghe, CEO -  Beira Enviro Solutions, “We are delighted to champion community action to ensure PET recycling and waste management with such a partner as Coca-Cola. We are grateful to the Kataragama Committee including the local authorities to having acknowledged the importance of setting an example for the community to take steps in mitigating their own individual waste footprint. Responsible disposal of post-consumed packages and recycling should be considered as a duty of all citizens in Sri Lanka. Beira looks forward to working with Coca-Cola to recycle PET bottles across the nation.”</p>
					</div>
<!-- 				<div>
				<a class="btn" href="downloads/Feb17 Interim Financials Release 22 March 2017.pdf" style="margin-left:15px;line-height:0.8;margin-bottom: 20px" download>Read More</a>
				</div>  -->
                </div>
			</div>	<!-- News Item 1 Ends -->

			<!-- News Item 2 Start -->		
			<div class="panel panel-primary" style="margin-top:25px;">
				<div class="panel-heading">
					<h3 class="panel-title">BPPL Holdings April 2016 to February 2017 Interim Financial Results</h3>
					<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-down"></i></span>
				</div>
                <div class="panel-collapse collapse out">
				    <div class="panel-body">

				    <h5><b>Net Profit of Rs393 million for the eleven months ended 28th February 2017</b></h5><br>

					<p>BPPL Holdings announced its interim unaudited financial results for the eleven month period April 2016 to February 2017. </p><br>
					<p>Consolidated revenue for the period was Rs2.2 billion, up 19% over the corresponding period in the previous year. Revenue continued to grow as the company pursued its dual objectives of penetrating the household market segment both through direct sales to retailers and own branded goods sales in Sri Lanka and Indonesia. Direct sales accounted for 11% of total sales for the period, up 30% year-on-year. Own branded goods also grew by 55%, again over the corresponding period in the previous year.</p><br>
					<p>Gross profit was up by a faster 30% year-on-year to Rs881 million due to margin expansion amid revenue growth. Gross profit margins, which improved from 37% to 40% during the eleven month period ended February 2017, continued to benefit from higher productivity, lower costs as a result of improved raw material sourcing and Sri Lankan Rupee depreciation against the US Dollar.  </p><br>
					<p>Improved productivity and stringent cost controls also led to a 47% increase in operating profit to Rs472 million compared to the same period in the previous year. Moreover, margins continued to expand to 21% at a Profit-Before-Tax level due to lower interest expenses as accumulated profits were used for debt retirement. Profit-Before-Tax was Rs453 million for the period whilst Profit-After-Tax attributable to the company’s shareholders was Rs393 million, an increase of 48% year-on-year. Non-annualized EPS for the period amounted to Rs1.28 (based on number of shares as at 28th February 2017). </p><br>
					<p>Meanwhile, BPPL Holdings moved ahead with its plans for extruding synthetic yarn by placing orders with machinery suppliers following successful trials conducted with its own hot washed recycled PET flakes and discussions with leading textile producers. The construction of a new factory in the Horana BOI Industrial Zone also commenced in January 2017. This yarn production facility, which involves an investment of Rs675 million, is set to come on-stream in the January to March quarter of 2018 and contribute to revenue from April 2018.</p><br>
					<p>The company is also on track to commence power generation from its own 347KW solar and 200KW biomass based power plants later this month. </p><br>
					<p>BPPL offered 30,685,000 ordinary shares priced at Rs12 per share to the public via an Initial Public Offering recently. The offer was fully subscribed on the first day. </p><br>
					</div>
<!-- 				<div>
				<a class="btn" href="downloads/Feb17 Interim Financials Release 22 March 2017.pdf" style="margin-left:15px;line-height:0.8;margin-bottom: 20px" download>Read More</a>
				</div>  -->
                </div>
			</div>	<!-- News Item 2 Ends -->
				
		</div>
	</div>
</div>



<style type="text/css">
	.row{
    margin-top:40px;
    padding: 0 10px;
}

.clickable{
    cursor: pointer;   
}

.panel-heading span {
	margin-top: -20px;
	font-size: 15px;
}
.panel-body{
	text-align: left;
}

.btn:hover{
	color: white;
}
</style>


<script type="text/javascript">
	
	$(document)
    .on('click', '.panel-heading span.clickable', function(e){
        $(this).parents('.panel').find('.panel-collapse').collapse('toggle');
    })
    .on('show.bs.collapse', '.panel-collapse', function () {
        var $span = $(this).parents('.panel').find('.panel-heading span.clickable');
        $span.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
    })
    .on('hide.bs.collapse', '.panel-collapse', function () {
        var $span = $(this).parents('.panel').find('.panel-heading span.clickable');
        $span.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
    })
</script>
</body>
</html>
