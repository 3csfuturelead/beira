<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?4HStZ39JpdCIf1dYpqIhUGF8BD0PBVmu";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
<style>
.dropbtn {
    background-color: rgba(76, 175, 80, 0);
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
    cursor: pointer;
    position: relative;
    display: inline-block;
    vertical-align: top;
    text-decoration: none;
    color: white;
    padding: 9px;
    text-align: center;
    margin-left: auto;
    margin-right: auto;
        font-weight: lighter !important;
}
.w-nav {
    position: fixed;
    background: rgba(10, 47, 115, 0.59);
    z-index: 1000;
    width: 97.01%;
    top: 0;
    height: 42px;
    line-height: 2.5;
    font-family: inherit;
    font-size: 16px;
    font-weight: 100;
}
.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
}

.dropdown-content a {
    color: black;
            padding: 4px 10px;
    text-decoration: none;
    display: block;
}

.dropdown-content a:hover {background-color:rgba(10, 47, 115, 0.59);}

.dropdown:hover .dropdown-content {
    display: block;
}

.dropdown:hover .dropbtn {
    background-color: rgb(85, 111, 161);;
}
</style>

  <?php //include("includes/bestweb.php"); ?>
 <?php
 $about_us_content= @content::get_maincontent_by_id(5);
 $contact_us_content= @content::get_maincontent_by_id(6);
  $factory_content= @content::get_maincontent_by_id(10);
$investor_content= @content::get_maincontent_by_id(11);
$directors_content= @content::get_maincontent_by_id(12);
$brands_content= @content::get_maincontent_by_id(13);


 // $tiptop_content=content::get_subcontent_by_id(2);
 $product_actegories_content= @content::get_active_product_categories();    // get the active product categories from tbl_product_category 

 /*
    Janitorial     |      1 |             1 | 2014-10-08 17:50:08 |
|  3 | Builders Range |      1 |             2 | 2014-10-08 17:50:57 |
|  4 | Household      |      1 |             4 | 2014-10-08 23:18:50 |
|  5 | Food Services  |      1 |             3 | 2014-10-09 09:00:12 |
|  6 | Tip Top        |      1 |             5 | 2014-11-10 13:21:42 |
|  7 | Blocks         |      1 |             5 | 2014-11-22 13:19:37 |
|  8 | Filaments
 */

 ?>

  <div class="w-nav accordion" data-collapse="medium" data-animation="default" data-duration="400" data-contain="1">
    <div class="w-container width-fix">
      <a class="w-nav-brand" href="/">
        <img class="logo" src="images/logo.png" alt="BPPL Holdings PLC">
      </a>
      <nav class="w-nav-menu accordion" role="navigation" style="width: initial; margin-right: 20px;">
      <!--<a class="w-nav-link" href="<?php echo HTTP_PATH; ?>">Home</a>-->
      <?php /* foreach ($product_actegories_content as $key => $row_content):
      $subcategory=content::get_first_subcat_by_mainid($row_content['id']);
        if ($subcategory) {
          $url=str_replace(' ', '', $row_content['title'])."_".str_replace(' ', '', $subcategory['heading']).".html";
        } else {
          $url=HTTP_PATH;
        }?>
        <a class="w-nav-link" href="<?php echo $url;?>"><?php echo $row_content['title'] ?></a>
      <?php endforeach */ ?>
      <!--<a class="w-nav-link" href="beira-Investor-Relations.html"><?php echo $investor_content['heading'];?></a>-->
      <!-- <a class="w-nav-link" href="beira-<?php echo str_replace(' ', '-', str_replace('&', 'and', $investor_content['heading']));?>.html"><?php echo $investor_content['heading'];?></a>      
      <a class="w-nav-link" href="brief-<?php echo str_replace(' ', '-', str_replace('&', 'and', $factory_content['heading']));?>.html"><?php echo $factory_content['heading'];?></a>
      <a class="w-nav-link" href="beira-<?php echo str_replace(' ', '-', str_replace('&', 'and', $about_us_content['heading']));?>.html"><?php echo $about_us_content['heading'];?></a>
      <a class="w-nav-link" href="beira-<?php echo str_replace(' ', '-', str_replace('&', 'and', $contact_us_content['heading']));?>.html"><?php echo $contact_us_content['heading'];?></a> 
<a class="w-nav-link" href="beira-search.html"><img class="magnify" src="images/search-icon-big.png" alt="search"></a>

-->
<!-- ==========  TEMP MENU ============= -->
       <div class="dropdown">
  <button class="dropbtn">
      <a class="w-nav-link" href="#" style="padding-right: 1px; margin-top: 1px;">Professional Range</a></button>
      <div class="dropdown-content">
    <a href="Janitorial_FineSweeps.html">Janitorial</a>
    <a href="BuildersRange_RoofingBrushes.html">Builders' Range</a>
    <a href="FoodServices_HygieneRange.html">Food Services</a>
  </div>
</div>

    

      <a class="w-nav-link" href="Household_AngleBrooms.html">Household</a>
      <a class="w-nav-link" href="beira-<?php echo str_replace(' ', '-', str_replace('&', 'and', $brands_content['heading']));?>.html"><?php echo $brands_content['heading'];?></a>
      <!--<a class="w-nav-link" href="#">Brands</a>-->
      
      <!--<div class="dropdown">
         <button class="dropbtn">
             <a class="w-nav-link" href="#">Brands</a></button>
                <div class="dropdown-content" style="min-width: 112px;">
                            <a href="sub-Tip-Top.html">Tip Top</a>
                            <a href="sub-JAB.html">JAB</a>
    
                </div>
          </div>-->
      <a class="w-nav-link" href="Blocks_WoodenBlocks.html">Blocks</a>
      <a class="w-nav-link" href="Filaments_VegetableFibres.html">Filaments</a>
      <a class="w-nav-link" href="brief-<?php echo str_replace(' ', '-', str_replace('&', 'and', $factory_content['heading']));?>.html"><?php echo $factory_content['heading'];?></a>
      <a class="w-nav-link" href="beira-<?php echo str_replace(' ', '-', str_replace('&', 'and', $about_us_content['heading']));?>.html"><?php echo $about_us_content['heading'];?></a>
      <a class="w-nav-link" href="beira-<?php echo str_replace(' ', '-', str_replace('&', 'and', $investor_content['heading']));?>.html"><?php echo $investor_content['heading'];?></a>
    <!--  <a class="w-nav-link" href="beira-<?php echo str_replace(' ', '-', str_replace('&', 'and', $directors_content['heading']));?>.html"><?php echo $directors_content['heading'];?></a> -->
      <a class="w-nav-link" href="beira-<?php echo str_replace(' ', '-', str_replace('&', 'and', $contact_us_content['heading']));?>.html"><?php echo $contact_us_content['heading'];?></a>
      <a class="w-nav-link" href="beira-search.html"><img class="magnify" src="images/search-icon-big.png" alt="search"></a>
<!-- ==========  //TEMP MENU ============= -->

      </nav>
      <div class="w-nav-button acoicon">
        <div class="w-icon-nav-menu acoicon"></div>
      </div>
    </div>
  </div>
<?php
  if (isset($_GET['main']) && $_GET['main']==5) {
    ?>
    <h2 class="range ab"> <?php echo $main['spe_details']; ?></h2>
    <?php
  }
  ?>

