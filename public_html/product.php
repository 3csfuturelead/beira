<?php
require_once('user.php');

if (! isset($_GET["val"]) || ! isset($_GET["type"])) {
	header("location: 404.html");
	exit();
}

$val = filter_var($_GET['val'], FILTER_SANITIZE_STRING);
$type = filter_var($_GET['type'], FILTER_SANITIZE_STRING);		// no need to sanitize this but just in case

if($val=="" || $type ==""){
	header("location: 404.html");
	exit();
}

$val = clean_text($val);		// this adds mysql_real_escape 
$val = trim($val);

if ($type=="product_list" && isset($_GET["val2"])) {
	$val2 = clean_text(filter_var($_GET["val2"], FILTER_SANITIZE_STRING));
	$val2 = trim($val2);
	$category_id = content::get_product_category_id_by_heading($val);
	$subcategory_title=$val2;
	$subcategory_content=content::get_product_subcategory_by_title($subcategory_title,$category_id['id']);
	if (!$subcategory_content) {
		header("location: 404.html");
		exit();
	}

	$all_products=content::get_active_products_by_subcategory($category_id['id'],$subcategory_content['id']);
	$allsubcategories=content::get_all_active_subcategories_by_categoryid($category_id['id']);

	$requirefile="product_list.php";

} elseif ($type=="product" && isset($_GET["val2"]) && isset($_GET["val3"])) {


	$val2 = clean_text(filter_var($_GET["val2"], FILTER_SANITIZE_STRING));
	$val2 = trim($val2);
	$val3 = clean_text(filter_var($_GET["val3"], FILTER_SANITIZE_STRING));
	$val3 = trim($val3);
	$category_id = content::get_product_category_id_by_heading($val);
	$subcategory_title=$val2;
	$subcategory_content=content::get_product_subcategory_by_title($subcategory_title,$category_id['id']);
	$product_content =content::get_product_content_from_title($category_id['id'],$subcategory_content['id'],$val3);
	if (!$subcategory_content || !$product_content) {
		header("location: 404.html");
		exit();
	}

	//$all_products=content::get_active_products_by_subcategory($category_id['id'],$subcategory_content['id']);
	//$allsubcategories=content::get_all_active_subcategories_by_categoryid($category_id['id'],$subcategory_title['id'],$val3);

	$requirefile="product_page.php";
} else {
	header("location: 404.html");
	exit();
}

$page_content=content::get_maincontent_by_id(8);

require_once("templates/productlist_template.php");
?>