<?php
require_once('user.php');
$home_details=content::get_maincontent_by_id(1);
?>
<?php include_once("detect.php"); 
//ini_set('display_errors','Off');
?>
<!DOCTYPE html>
<html class="tk-myriad-pro">
<head>
<?php include_once("meta_tags.php"); ?>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WTWZQ8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WTWZQ8');</script>
<!-- // End Google Tag Manager -->
<!--
	<meta charset="utf-8">
	<title><?php echo $home_details["meta_title"]; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="generator" content="Webflow"> -->

	<link rel="shortcut icon" type="image/x-icon" href="images/favic.jpg">
	<link rel="apple-touch-icon" href="images/webclip.jpg">
	
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  
	<link rel="stylesheet" type="text/css" href="css/normalize.min.css">
	<link rel="stylesheet" type="text/css" href="css/webflow.css">
	<link rel="stylesheet" type="text/css" href="css/beiragroup.webflow.css">
	<link rel="stylesheet" type="text/css" href="css/styles.css">
	<link rel="stylesheet" type="text/css" href="webfont/stylesheet.min.css">
	<link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
	<script src="//use.typekit.net/mrg7dwk.js"></script>
  	<script>try{Typekit.load();}catch(e){}</script>
  	<?php include_once("ie-fix.php"); ?>
	
	<script src="js/webfont.js"></script>
	
<style>
@media screen and (max-width: 1380px) and (min-width: 1330px) { 
.container {
    
    left: -516px !important;
   
    top: -611px !important;
}

}
@media screen and (max-width: 1440px) and (min-width: 1400px) { 
.container {
    
    left: -536px !important;
   
    top: -601px !important;
}

}
@media screen and (max-width: 1280px) and (min-width: 1200px) { 
.container {
    
    left: -482px !important;
   
    top: -601px !important;
}

}

#container {
  max-width: 1000px;
  margin: 0 auto;
  background: #EEE;
}


p { padding: 1em 1em; }

#fvpp-blackout {
  display: none;
  z-index: 499;
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  background: #000;
  opacity: 0.5;
}

#my-welcome-message {
  display: none;
  z-index: 500;
  position: fixed;
  width: 53%;
  left: 23%;
  top: 16%;
  padding: 20px 2%;
  font-family: Calibri, Arial, sans-serif;
  background: #FFF;
}

#fvpp-close {
  position: absolute;
  top: 10px;
  right: 20px;
  cursor: pointer;
}

#fvpp-dialog h2 {
  font-size: 2em;
  margin: 0;
}

#fvpp-dialog p { margin: 0; }
.container {
    /* position: relative; */
    width: 16%;
    left: -719px;
    display: block;
    margin-left: auto;
    margin-right: auto;
    padding: -6px;
    text-align: center;
    position: relative;
    max-width: 1400px;
    z-index: +6;
    top: -729px;
    
}
h3 {
    margin-top: 0px;
    margin-bottom: 0px;
    font-size: 24px;
    line-height: 30px;
    font-weight: 700;
}
h1{margin-top: 0px;
    margin-bottom: 0px;}

</style>

<link rel="stylesheet" type="text/css" href="css/banner.css">

</head>


<body>



	<div class="header">
	
		<?php
		require_once('header.php');
	$main_contents=content::get_active_main_content();
	?>
	

	</div>
	<div class="body">
		<div class="body-container">
			<?php
			foreach ($main_contents as $key => $row) {
				$alt = array(2=>'Brush Manufacturer Sri Lanka',3=>'BPPL Holdings PLC, Health and Safety',4=>'Recycling Projects Sri Lanka');
				?>
				<div class="fixer w-inline-block">
					<a class="w-inline-block catagory" id="main_content_box_<?php echo $key+1;?>" href="brief-<?php echo str_replace(' ', '-', str_replace('&', 'and', $row['heading']));?>.html">
						<img class="cat-image" src="images/content/banner/<?php echo $row['image3'];?>" alt="<?php echo $alt[$row['maincontent_id']];?>">
						<div class="cat-title" id="a<?php echo $key+1;?>"><?php echo $row['heading'];?></div>
					</a>
					<img class="shadow" src="images/shadow.png" alt="shadow">
				</div>
				<?php
			}
			?>
				<div class="fixer w-inline-block orange">
					<a class="w-inline-block catagory" id="main_content_box_4" href="beira-Latest-News.html">
						<img class="cat-image" src="images/content/banner/latest_news.jpg" alt="Beira Group News">
						<div class="cat-title" id="a4">Latest News</div>
					</a>
					<img class="shadow" src="images/shadow.png" alt="shadow">
				</div>
				<!-- <div class="fixer w-inline-block gray">
					<a class="w-inline-block catagory" id="main_content_box_5" href="brief-Share-Price.html">
						<img class="cat-image" src="images/content/banner/share_price.jpg" alt="Share Price">
						<div class="cat-title" id="a5">Share Price</div>
					</a>
					<img class="shadow" src="images/shadow.png" alt="shadow">
				</div> -->

		</div>

	</div>
<!-- <div style="width:100%; background:#e5f8a1; height: 40px; font-size: 20px;"><marquee style="line-height: 35px;">test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test </marquee></div>-->
	<!-- footer start -->
	<div class="footerI" style="bottom:0;">
	<div class="copyright">

	<a  href="<?php echo HTTP_PATH; ?>">home | </a>
	<a  href="beira-<?php echo str_replace(' ', '-', str_replace('&', 'and', $about_us_content['heading']));?>.html">careers | </a>
	<a  href="beira-<?php echo str_replace(' ', '-', str_replace('&', 'and', $contact_us_content['heading']));?>.html">contact us | </a>
	<!-- <a class="footer" href="#">privacy policy |  </a> -->
	<a  href="beira-Site-Map.html">site map | </a>

	<script type="text/javascript">
	    var date = new Date();
	    var year = date.getYear();
	    year = year < 1000 ? year + 1900 : year;
	    document.write('&copy;' + '&nbsp;' + year);
	</script>
	Beira Group |
	<a  href="http://www.3cs.lk">Web Design by 3CS</a>
	</div>
	</div>
	<!-- footer ends -->


	<!--[if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"><![endif]-->


	<script type="text/javascript" src="js/jquery1.11.1.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<!--<script src="js/popup.js"></script>-->


	<!-- <script>
	    $(document).ready(function() {

	      // Initialize the plugin
	      $('#my_popup').popup({
	      	<?php if(!isset($_SESSION['per_user']) && $_SESSION['per_user']!="TRUE"){ ?>
	        autoopen: true
	        <?php } ?>
	      });

	    });
	  </script>-->
	  
	  <!--   =========== SWITCH THE LATEST NEWS AND SHARE PRICE ================  -->
	<script>
	/*	$('.orange').hide();

		window.setInterval(switchDiv, 5000);

		  function switchDiv() 
		  {
		  	$('.gray, .orange').toggle()
		  } */
	</script>
	<!--   =========== //SWITCH THE LATEST NEWS AND SHARE PRICE ================  -->


	<script>
		$('#form').mouseover(function(){
			$('#input').animate({opacity:'1',width:'101px'},'linear');
		});
		$('#btn').click(function(){
			$('#input').animate({width:'0px',opacity:'0'},'linear');
		});
	</script>

	<?php //echo $seo_code; ?>
	<script>
		WebFont.load({
			google: {
				families: ["Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic","Ubuntu:300,300italic,400,400italic,500,500italic,700,700italic"]
			}
		});
	</script>
	
	<script type="text/javascript" src="js/modernizr.js"></script>
	<script type="text/javascript" src="js/webflow.js"></script>
	<!--<div id="my_popup">

    		<img class="award" src="images/award.jpg" alt="award">
    		<button class="my_popup_close">X</button>

  	</div>-->

<!-- POP UP Window  
<div id="ipopopup" class="promo-banner mfp-hide" style="text-align: center; position: relative;">
	<img src="images/Front_banner.jpg" alt="Notice">
	<h6 style="position: absolute; top: 520px; left: -260px; width: 100%;"><a href="beira-Investor-Relations.html" style="color: black;text-decoration:none;">Click to view more on the IPO</a></h6>
</div> -->

<div id="ipobanner" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="position: relative;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Notice</h4>
      </div>
      <img src="images/Front_banner.jpg" alt="Notice">
      <span style="position: absolute; bottom: 8%; left: 8%;"><a href="beira-Investor-Relations.html">Click to view more on the IPO</a></span>
    </div>
  </div>
</div>



<?php
	$_SESSION['per_user']="TRUE";
?>



<!-- POP UP JS -->
<!-- <script src="js/jquery.magnific-popup.min.js"></script>-->
<?php if(!isset($_COOKIE['LAST_VISITED'])): ?>
<!--
<script>
(function($) {
    $(window).load(function () {
        // retrieved this line of code from http://dimsemenov.com/plugins/magnific-popup/documentation.html#api
        $.magnificPopup.open({
            items: {
                src: '#ipopopup'
            },
            type: 'inline',
            closeBtnInside: true,
            closeOnBgClick: true

        }, 0);
    });
})(jQuery);
</script> 
-->


<!--	<script type="text/javascript">
		$('#ipobanner').modal({
			show: true
		})
	</script>
-->


<?php endif; ?>
<!--End  POP UP JS -->

</body>
</html>