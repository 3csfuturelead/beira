<?php include_once("detect.php"); ?>
<!doctype html>
<html style="width:100%; height:100%;" class="tk-myriad-pro">
<head>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WTWZQ8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WTWZQ8');</script>
<!-- End Google Tag Manager -->
<!--
  <meta charset="utf-8">
  <meta name="description" content="<?php echo $page_content['meta_desc']; ?>">
  <meta name="keywords" content="<?php echo $page_content['meta_key']; ?>">
  <title><?php echo $page_content['meta_title']; ?></title> -->
  <link rel="shortcut icon" type="image/x-icon" href="images/favic.jpg">
  <link rel="apple-touch-icon" href="images/webclip.jpg">
 <script type="text/javascript" src="js/jquery1.11.1.js"></script>
  <link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
 <script src="//use.typekit.net/mrg7dwk.js"></script>
  <script>try{Typekit.load();}catch(e){}</script>
 <?php //echo $seo_code; ?>

 <style type="text/css">

 .certified-logos {
    margin-left: 30px;
 }

  .certified-logos ul{
    list-style: none;
    display: block;
    margin-left: 0px;
    padding-left: 0px;
  }

  .certified-logos ul li{
    display: inline;
    margin-right: 23px;
  }

  .certified-logos ul li:first-chilid{
    display: block;
  }

  .certified-logos ul li img {

  }

  .certified-logos p{
    margin-bottom: 10px;
  }
 </style>
 
</head>


<body>
<?php if ( (isset($_GET["main"])) && $_GET["main"]==6) {
  ?>
  <iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.7799267378587!2d79.85858900000001!3d6.916893999999985!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ae2596c61b2d731%3A0x605392961daacf55!2sThe+Beira+Group!5e0!3m2!1sen!2slk!4v1408614661967" frameborder="0" style="border:0"></iframe>
  <div class="adjust"><?php include_once("header.php"); ?></div>
  <?php
} else {
  ?>
  <div class="head" style="background-image:url(images/content/banner/<?php echo (isset($_GET['brief']) && $_GET['brief']==true) ? $page_content['image2'] : $page_content['image1'] ; ?>); background-size:cover; -ms-behavior: url(backgroundsize.min.htc);">
    <?php include_once("header.php"); ?>
  </div>
  <?php
}
?>

  <?php
  if (isset($_GET['brief']) && $_GET['brief']==true) {
    ?>
    <div class="qbox">
      <h1 class="inner-h1"><?php echo $page_content['spe_heading']; ?></h1>
      <p class="inner-small-despript"><?php echo $page_content['spe_details']; ?></p>
      <!--<img src="images/logos.jpg" alt="certificates" style="display;block; width:38%; vertical-align:middle; margin-left:27px; margin-bottom:7px;">-->
      <div class="certified-logos">
        <p>
            <a href="resources/pdf/FSC.pdf" title="Click to view the certificate" target="_blank"><img src="images/certifiedlogos/FSC_100x143.fw.png" alt="FSC"></a><br><br>
            <span><a href="mailto:info@beiragroup.com">Ask About Our FSC Certified Products</a></span>
        </p>
        <hr>
        <ul>
          <li><a href="resources/pdf/ISO9001_certificate.pdf" title="Click to view the certificate" target="_blank"><img src="images/certifiedlogos/iso9001_60.jpg" alt="ISO 9001"></a></li>
          <li><a href="resources/pdf/ISO14001_certificate.pdf" title="Click to view the certificate" target="_blank"><img src="images/certifiedlogos/iso14001_60.jpg" alt="ISO 14001"></a></li>
          <li><a href="resources/pdf/New Certificate OHSAS 18001-2007.pdf" title="Click to view the certificate" target="_blank"><img src="images/certifiedlogos/ohsas18001_60.jpg" alt="OHSAS 18001"></a></li>
          <li><img src="images/certifiedlogos/usgbc_60.jpg" alt="USGBC"></li>
          <li><img src="images/certifiedlogos/naturaltimber_60.jpg" alt="Natural Timber"></li>
        </ul>
        <br>
        <a href="resources/pdf/Certificate_of_Registration_ISPM.pdf" title="Click to view the certificate" target="_blank"><img src="images/certifiedlogos/IPPC_logo.fw.png" alt=""></a>
        <p style="font-size: 10px;">
          Click on a logo to see its certification
        </p>
      </div>
      <a class="btn" href="beira-<?php echo str_replace(' ', '-', str_replace('&', 'and', $page_content['heading']));?>.html" style="margin-left:30px">Read More</a>
    </div>

    <div class="cycle-slideshow" data-cycle-fx="tileSlide"  data-cycle-tile-vertical=false data-cycle-speed="700" data-cycle-delay="500" style="display:block; float:left; margin-left:9%; margin-top:86px; max-width:45%;">
    <?php
    $slider_images_array=content::get_active_slider_images($_GET['main']);
    foreach ($slider_images_array as $key => $slider_image_row) {
      ?>
      <img class="slide" src="images/content/slider/<?php echo $slider_image_row["image_name"]; ?>" alt="<?php echo $key;?>">
      <?php
    }
     ?>
   </div>
    <?php
  } else {
    ?>

<div class="container">
  <?php
  if ((isset($_GET['main']) && ($_GET['main']!=5 && $_GET['main']!=6)) || isset($_GET['sub'])  ) { //no body header in aboutus and contact us
    ?>
    <h3 class="Chead" style="margin-left:10%; margin-top:20px; text-align:left;"><?php echo $page_content['heading']; ?></h3>
    <?php
  }
  ?>

  <?php
    if ($requirefile!="") { // include body file
      include $requirefile;
    } else{
      ?>
      <div class="content"><?php echo $page_content['details']; ?></div>
      <?php
    }
  ?>
</div>

    <?php
  }
  ?>

  <?php include_once("footer.php"); ?>
  <?php include_once("ie-fix.php"); ?>
  <link rel="stylesheet" type="text/css" href="css/beiragroup.webflow.css">
  <link rel="stylesheet" type="text/css" href="css/styles.css">
  <link rel="stylesheet" type="text/css" href="css/normalize.css">
  <link rel="stylesheet" type="text/css" href="css/webflow.css">
  <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css">

   
  <script type="text/javascript" src="js/modernizr.js"></script>
  <script src="js/webfont.js"></script>
  <script>
    WebFont.load({
      google: {
        families: ["Lato:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic","Ubuntu:300,300italic,400,400italic,500,500italic,700,700italic"]
      }
    });
  </script>
  <link rel="shortcut icon" type="image/x-icon" href="images/favic.jpg">
  <link rel="apple-touch-icon" href="images/webclip.jpg">

  <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
  <script type="text/javascript" src="js/webflow.js"></script>
  <script src="js/jquery.cycle2.min.js"></script>
  <script src="js/jquery.cycle2.tile.min.js"></script>
  
  <script>
    (function($){
      $(window).load(function(){
        $(".content").mCustomScrollbar({
         scrollButtons:{enable:true,scrollType:"stepped"},
         keyboard:{scrollType:"stepped"},
         theme:"dark-thin",
         autoExpandScrollbar:true,
         snapAmount:188,
         snapOffset:65
       });
      });
    })(jQuery);
  </script>

 
  <script>
  	$('#form').mouseenter(function(){
      $('#input').animate({opacity:'1',width:'101px'},'linear');
    });
   $('#btn').click(function(){
    $('#input').animate({width:'0px',opacity:'0'},'linear');
  });





 </script>
 <script>
   
  $(window).scroll(function(){
	$('.w-nav').css("background-color","rgb(93, 121, 172)");
	});
  
  </script>
<link rel="stylesheet" type="text/css" href="webfont/stylesheet.css">

<!-- POP UP JS -->
<script src="js/jquery.magnific-popup.min.js"></script>

<?php if(!isset($_COOKIE['LAST_VISITED'])): ?>
<!--
<script>
(function($) {
    $(window).load(function () {
        // retrieved this line of code from http://dimsemenov.com/plugins/magnific-popup/documentation.html#api
        $.magnificPopup.open({
            items: {
                src: 'images/main-popup.jpg'
            },
            type: 'image'

        }, 0);
    });
})(jQuery);
</script>
-->
<?php endif; ?>
<!--End  POP UP JS -->
</body>
</html>